package com.example.piotrek.monitoring.Adapters;

/**
 * Created by Piotrek on 20.01.2018.
 */

public interface StepListener {

    public void step(long timeNs);

}