package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.piotrek.monitoring.Data.Contact_data.UserNotification;
import com.example.piotrek.monitoring.R;

import de.cketti.mailto.EmailIntentBuilder;

/**
 * Created by Piotrek on 11.01.2018.
 */

public class EventFullInfo extends AppCompatActivity {

    TextView tvTitle, tvDesc, tvDate, tvFullInfo;
    Button btnSendMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_full_info);
        btnSendMail = findViewById(R.id.btnSendMail);


        tvTitle = (TextView) findViewById(R.id.tvFullTitle);
        tvDesc = (TextView) findViewById(R.id.tvTitle);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvFullInfo = (TextView) findViewById(R.id.tvFullDesc);

        final Intent intent = getIntent();
        tvTitle.setText(intent.getStringExtra("title"));
        tvDate.setText("Data: " + intent.getStringExtra("date"));
        tvFullInfo.setText(intent.getStringExtra("full"));

        btnSendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
                try {
                    boolean success = EmailIntentBuilder.from(getApplicationContext())
                            .to(intent.getStringExtra("contact"))
                            .subject(tvTitle.getText().toString() + " " + tvDate.getText().toString())
                            .body(tvFullInfo.getText().toString())
                            .start();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "Brak zainstalowanych klientów poczty.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
