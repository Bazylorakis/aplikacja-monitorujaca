package com.example.piotrek.monitoring.Adapters;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 20.01.2018.
 */

public class StepsTemplate {

    public int mReadingId;
    public String mUserId;
    public int mStepsCounter;
    public String mStepsDate;



    public StepsTemplate() {
    }

    public StepsTemplate(String userId, String date, int stepsCounter) {
        this.mUserId = userId;
        this.mStepsCounter = stepsCounter;
        this.mStepsDate = date;
    }

    public static ArrayList<StepsTemplate> makeStepsReading() {
        ArrayList<StepsTemplate> stepsReadings = new ArrayList<>();
        return stepsReadings;
    }


    public ArrayList<StepsTemplate> getStepsReading() {
        ArrayList<StepsTemplate> stepsReadings = new ArrayList<>();
        return stepsReadings;
    }

}