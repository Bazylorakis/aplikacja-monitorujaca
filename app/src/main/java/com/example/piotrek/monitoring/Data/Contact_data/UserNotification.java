package com.example.piotrek.monitoring.Data.Contact_data;

import java.util.ArrayList;

/**
 * Created by Piotrek on 19.01.2018.
 */

public class UserNotification {
    public String mNotificationTitle;
    public String mNotificationTime;

    public UserNotification() {
    }

    public UserNotification(String title, String time) {
        this.mNotificationTitle = title;
        this.mNotificationTime = time;
    }


    public ArrayList<UserNotification> getNotifications() {
        ArrayList<UserNotification> notifications = new ArrayList<>();
        return notifications;
    }

}
