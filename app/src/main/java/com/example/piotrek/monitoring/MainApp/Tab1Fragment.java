package com.example.piotrek.monitoring.MainApp;


import android.content.Intent;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Piotrek on 27.10.2017.
 */

public class Tab1Fragment extends Fragment {

    private static final String TAG = "Tab1Fragment";


    EditText etLogin;
    EditText etPassword;
    Button btn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        etLogin = view.findViewById(R.id.etLogin);
        etPassword = view.findViewById(R.id.etPassword);


        Button loginBtn = view.findViewById(R.id.btnLogin);


        TextView tvRegister = view.findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ViewPager vp = getActivity().findViewById(R.id.container);
                vp.setCurrentItem(1);
            }
        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String login = etLogin.getText().toString();
                String password = etPassword.getText().toString();
                Boolean requestLogin = true;
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                intent.putExtra("requestLogin", requestLogin);
                intent.putExtra("login", login);
                intent.putExtra("pass", password);
                startActivityForResult(intent, 1);

            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            } else if (resultCode == 2) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Błąd logowania.");
                builder.show();
                etLogin.setText("");
                etPassword.setText("");
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
