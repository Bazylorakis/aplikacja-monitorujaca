package com.example.piotrek.monitoring.Data.Contact_data;

import android.util.Log;

import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.R.attr.name;

/**
 * Created by Piotrek on 07.01.2018.
 */

public class Event implements Comparable<Event>{
    public String mEventName;
    public String mEventDesc;
    public String mEventDate;
    public Date date;

    public Event() {
    }

    public Event(String eventName, String eventDesc, String eventDate) throws ParseException {
        this.mEventName = eventName;
        this.mEventDesc = eventDesc;
        this.mEventDate = eventDate;
        DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date = sourceFormat.parse(this.mEventDate);
        this.date = date;
        Log.d("GETDATE", this.date.toString());
    }

    public Event newEvent(String eventName, String eventDesc, String eventDate) throws ParseException {
        Event event = new Event(eventName, eventDesc, eventDate);
        return event;
    }

    public ArrayList<Event> getEvents() {
        ArrayList<Event> events = new ArrayList<>();
        return events;
    }


    public Date getDate(){
        return date;
    }
    @Override
    public int compareTo(Event o){
        return o.getDate().compareTo(getDate());
    }


}
