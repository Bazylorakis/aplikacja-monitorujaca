package com.example.piotrek.monitoring.UserInterface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.piotrek.monitoring.BackgroundTasks.AddContactAsync;
import com.example.piotrek.monitoring.BackgroundTasks.AddNewEvent;
import com.example.piotrek.monitoring.BackgroundTasks.BasicParamsUploadAsync;
import com.example.piotrek.monitoring.BackgroundTasks.FirstLoginAsync;
import com.example.piotrek.monitoring.BackgroundTasks.GetBasicReadingsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.GetDataForRange;
import com.example.piotrek.monitoring.BackgroundTasks.GetDayStatistics;
import com.example.piotrek.monitoring.BackgroundTasks.GetEventsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.LoginAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RegisterAsync;

import com.example.piotrek.monitoring.BackgroundTasks.UpdateProfile;
import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Events;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.R.attr.data;
import static android.R.attr.name;

/**
 * Created by Piotrek on 03.01.2018.
 */

public class WaitInBackground extends Activity implements AsyncResponse {

    ProgressBar pb;
    TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waitinbackground);



        Intent intent = getIntent();
        if (intent.getBooleanExtra("requestLogin", false)) {
            String login = intent.getStringExtra("login");
            String password = intent.getStringExtra("pass");
            LoginAsync sendPostRequest = new LoginAsync(getApplicationContext(), "login");
            sendPostRequest.delegate = WaitInBackground.this;
            sendPostRequest.execute(login, password);
        } else if (intent.getBooleanExtra("requestRegister", false)) {
            String str_login = intent.getStringExtra("reg_login");
            String str_password = intent.getStringExtra("reg_pass");
            String str_email = intent.getStringExtra("email");
            RegisterAsync sendPostRequest = new RegisterAsync(getApplicationContext(), "register");
            sendPostRequest.delegate = WaitInBackground.this;
            sendPostRequest.execute(str_login, str_password, str_email);
        }
        else if (intent.getBooleanExtra("firstLogin", false)) {
            String name = intent.getStringExtra("name");
            String surname = intent.getStringExtra("surname");
            String email = intent.getStringExtra("email");
            String age = intent.getStringExtra("age");
            String pesel = intent.getStringExtra("pesel");
            String id = intent.getStringExtra("id");
            String contact = intent.getStringExtra("contact");
            String firstTime = Integer.toString(intent.getIntExtra("firstTime", 1));
            FirstLoginAsync sendPostRequest = new FirstLoginAsync(getApplicationContext(), "firstLogin");
            sendPostRequest.delegate = WaitInBackground.this;
            sendPostRequest.execute(id, name, surname, email, age, pesel, firstTime, contact);
        }
        else if (intent.getBooleanExtra("requestUpdate", false)) {
            String name = intent.getStringExtra("name");
            String surname = intent.getStringExtra("surname");
            String email = intent.getStringExtra("email");
            String age = intent.getStringExtra("age");
            String pesel = intent.getStringExtra("pesel");
            String id = intent.getStringExtra("user_id");
            String medic = intent.getStringExtra("contact");
            UpdateProfile sendPostRequest = new UpdateProfile(getApplicationContext(), "requestUpdate");
            sendPostRequest.delegate = WaitInBackground.this;
            sendPostRequest.execute(id, name, surname, email, age, pesel, medic);
        }
        else if (intent.getBooleanExtra("sendDayData", false)) {
            String id = intent.getStringExtra("user_id");
            String data = intent.getStringExtra("data");
            String pure_data = intent.getStringExtra("pure_data");
            String pulse = Integer.toString(intent.getIntExtra("pulse", 0));
            String breath = Integer.toString(intent.getIntExtra("breath", 0));
            String skurcz = Integer.toString(intent.getIntExtra("skurcz", 0));
            String rozkurcz = Integer.toString(intent.getIntExtra("rozkurcz", 0));
            String temp = Double.toString(intent.getDoubleExtra("temp", 0.0));
            BasicParamsUploadAsync basicParamsUploadAsync = new BasicParamsUploadAsync(getApplicationContext(), "sendDayData");
            basicParamsUploadAsync.delegate = WaitInBackground.this;
            basicParamsUploadAsync.execute(id, data, pulse, breath, skurcz, rozkurcz, temp, pure_data);
        }
        else if (intent.getBooleanExtra("requestAddEvent", false)) {
            String id = intent.getStringExtra("user_id");
            String data = intent.getStringExtra("eventDate");
            String eventName = intent.getStringExtra("eventName");
            String eventDesc = intent.getStringExtra("eventDesc");
            AddNewEvent addNewEvent = new AddNewEvent(getApplicationContext(), "addEventData");
            addNewEvent.delegate = WaitInBackground.this;
            addNewEvent.execute(id, data, eventName, eventDesc);
        }
        else if (intent.getBooleanExtra("requestAddNumber", false)) {
            String id = intent.getStringExtra("user_id");
            String name = intent.getStringExtra("name");
            String surname = intent.getStringExtra("surname");
            String number = intent.getStringExtra("number");
            AddContactAsync addContactAsync = new AddContactAsync(getApplicationContext(), "addContactNumber");
            addContactAsync.delegate = WaitInBackground.this;
            addContactAsync.execute(id, name, surname, number);
        } else if (intent.getBooleanExtra("requestRefreshEvents", false)) {
            String id = intent.getStringExtra("user_id");
            GetEventsAsync getEventsAsync1 = new GetEventsAsync(getApplicationContext(), "getEvents");
            getEventsAsync1.delegate = WaitInBackground.this;
            getEventsAsync1.execute(id);
        } else if (intent.getBooleanExtra("requestRefreshReadings", false)) {
            String id = intent.getStringExtra("user_id");
            GetBasicReadingsAsync getBasicReadingsAsync = new GetBasicReadingsAsync(getApplicationContext(), "getReadings");
            getBasicReadingsAsync.delegate = WaitInBackground.this;
            getBasicReadingsAsync.execute(id);
        } else if (intent.getBooleanExtra("requestDayData", false)) {
            String id = intent.getStringExtra("id");
            String date = intent.getStringExtra("date");
            GetDayStatistics getDayStatistics = new GetDayStatistics(getApplicationContext(), "getStatistics");
            getDayStatistics.delegate = WaitInBackground.this;
            getDayStatistics.execute(id, date);
        }
        else if (intent.getBooleanExtra("requestDataRange", false)) {
            String id = intent.getStringExtra("user_id");
            GetDataForRange getDataForRange = new GetDataForRange(getApplicationContext(), "getDataRange");
            getDataForRange.delegate = WaitInBackground.this;
            getDataForRange.execute(id);
        }
    }


    @Override
    public void processFinish(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_login");
            if (response) {
                String user_id = jsonObject.getString("user_id");
                String login = jsonObject.getString("login");
                String pass = jsonObject.getString("password");
                String email = jsonObject.getString("email");
                String name = jsonObject.getString("name");
                String surname = jsonObject.getString("surname");
                String pesel = jsonObject.getString("pesel");
                String age = jsonObject.getString("age");
                String medic = jsonObject.getString("userContactMedic");
                int first_login = jsonObject.getInt("first_login");
                Intent intent = new Intent(WaitInBackground.this, UserAreaActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("login", login);
                intent.putExtra("password", pass);
                intent.putExtra("email", email);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("pesel", pesel);
                intent.putExtra("age", age);
                intent.putExtra("first_login", first_login);
                intent.putExtra("contact", medic);
                startActivity(intent);
                setResult(RESULT_OK);
                finish();
            } else {
                setResult(2);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else {
                setResult(4);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("updated");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else {
                setResult(4);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("profile_updated");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else {
                setResult(1137);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("data_uploaded");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("event_uploaded");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else if (output == "False: 502") {
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_contact");
            if (response) {
                setResult(RESULT_OK);
                finish();
            } else if (output == "False: 502") {
                setResult(RESULT_CANCELED);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getEvents");
            if (response) {
                Intent intent = new Intent();
                intent.putExtra("output", output);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getBasics");
            if (response) {
                Intent intent = new Intent();
                intent.putExtra("output", output);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getStatistics");
            if (response) {
                Intent intent = new Intent();
                intent.putExtra("output", output);
                intent.putExtra("date", data);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getDataRange");
            if (response) {
                Intent intent = new Intent();
                intent.putExtra("output", output);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processStart(String input) {

    }
}
