package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


/**
 * Created by Piotrek on 13.01.2018.
 */

public class PressureView extends android.support.v4.app.Fragment {

    ArrayList<Integer> skurcz;
    ArrayList<Integer> rozkurcz;
    ArrayList<Double> time;


    GraphView graphView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pressure_view, container, false);


        graphView = view.findViewById(R.id.pressureGraph);


        Intent intent = getActivity().getIntent();

        String array = intent.getStringExtra("array");

        Log.d("Json jestem", array);
        skurcz = new ArrayList<>();
        rozkurcz = new ArrayList<>();
        time = new ArrayList<>();

        try {
            LineGraphSeries<DataPoint> series;
            LineGraphSeries<DataPoint> series2;
            JSONArray jsonArray = new JSONArray(array);
            for (int i = 0; i < jsonArray.length(); i++) {
                Log.d("Json jestem", "Tak");
                if (!jsonArray.getJSONObject(i).getString("skurcz").matches(""))
                    skurcz.add(Integer.parseInt(jsonArray.getJSONObject(i).getString("skurcz")));
                if (!jsonArray.getJSONObject(i).getString("rozkurcz").matches(""))
                    rozkurcz.add(Integer.parseInt(jsonArray.getJSONObject(i).getString("rozkurcz")));
                if (!jsonArray.getJSONObject(i).getString("time_of_reading").matches("")) {
                    String toReplace = jsonArray.getJSONObject(i).getString("time_of_reading");
                    char[] replace = toReplace.toCharArray();
                    replace[2] = '.';
                    toReplace = String.valueOf(replace);
                    time.add(Double.parseDouble(toReplace));
                }
            }

            int size = skurcz.size();
            DataPoint[] values = new DataPoint[size];
            for (int i = 0; i < size; i++) {
               /* Double xi = time.get(i);*/
                Double xi = time.get(i);
                Integer yi = skurcz.get(i);
                DataPoint v = new DataPoint(xi, yi);
                values[i] = v;
            }

            int size2 = rozkurcz.size();
            DataPoint[] values2 = new DataPoint[size2];
            for (int i = 0; i < size; i++) {
                Double xi = time.get(i);
                Integer yi = rozkurcz.get(i);
                DataPoint v2 = new DataPoint(xi, yi);
                values2[i] = v2;
            }

            series = new LineGraphSeries<DataPoint>(values);
            series2 = new LineGraphSeries<>(values2);

            series2.setColor(Color.RED);

            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMinY(40);
            graphView.getViewport().setMaxY(200);


            graphView.getViewport().setXAxisBoundsManual(true);
            graphView.getViewport().setMinX(0);
            graphView.getViewport().setMaxX(24);


            series.setDrawDataPoints(true);
            series2.setDrawDataPoints(true);

            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getContext(), "Ciśnienie skurczowe.\nGodzina: " + dataPoint.getX() +"\nWartość: "+ dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });
            series2.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getContext(), "Ciśnienie rozkurczowe.\nGodzina: " + dataPoint.getX() +"\nWartość: "+ dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });

            graphView.getGridLabelRenderer().setNumHorizontalLabels(24);

            graphView.addSeries(series);
            graphView.addSeries(series2);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
