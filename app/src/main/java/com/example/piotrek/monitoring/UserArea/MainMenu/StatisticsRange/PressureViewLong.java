package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 22.01.2018.
 */

public class PressureViewLong extends android.support.v4.app.Fragment {


    ArrayList<Integer> skurcz;
    ArrayList<Integer> rozkurcz;
    ArrayList<Date> time;

    GraphView graphView;
    DateFormat sdf;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_breath_view_long, container, false);

        StatisticsRangeMenu statisticsRangeMenu = (StatisticsRangeMenu) getActivity();

        graphView = view.findViewById(R.id.breathGraph);

        skurcz = new ArrayList<>();
        rozkurcz = new ArrayList<>();
        time = new ArrayList<>();

        for(int i = 0; i<statisticsRangeMenu.averages.size(); i++){
            skurcz.add(statisticsRangeMenu.averages.get(i).avgSkurcz);
            rozkurcz.add(statisticsRangeMenu.averages.get(i).avgRozkurcz);
            time.add(statisticsRangeMenu.averages.get(i).date);
        }

        LineGraphSeries<DataPoint> series;
        LineGraphSeries<DataPoint> series2;
        int size = skurcz.size();
        DataPoint[] values = new DataPoint[size];
        for (int i = 0; i < size; i++) {
            Date xi = time.get(i);
            Integer yi = skurcz.get(i);
            DataPoint v = new DataPoint(xi, yi);
            values[i] = v;
        }
        int size2 = rozkurcz.size();
        DataPoint[] values2 = new DataPoint[size2];
        for (int i = 0; i < size; i++) {
            Date xi = time.get(i);
            Integer yi = rozkurcz.get(i);
            DataPoint v2 = new DataPoint(xi, yi);
            values2[i] = v2;
        }
        series = new LineGraphSeries<DataPoint>(values);
        series2 = new LineGraphSeries<>(values2);
        series2.setColor(Color.RED);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(200);
        sdf = new SimpleDateFormat("dd-MM-yyyy");
        graphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getContext(), sdf));
        graphView.getGridLabelRenderer().setNumHorizontalLabels(time.size());
        graphView.getViewport().setMinX(series.getLowestValueX());
        graphView.getViewport().setMaxX(series.getHighestValueX());
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getGridLabelRenderer().setHumanRounding(false);
        series.setDrawDataPoints(true);
        series2.setDrawDataPoints(true);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getContext(), "Średnie ciśnienie skurczowe.\nData: "
                        + sdf.format(dataPoint.getX())+"\nWartość: "
                        + dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });
        series2.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getContext(), "Średnie ciśnienie rozkurczowe.\nData: "
                        + sdf.format(dataPoint.getX())+"\nWartość: "
                        + dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });
        graphView.addSeries(series);
        graphView.addSeries(series2);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}