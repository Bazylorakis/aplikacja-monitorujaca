package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.example.piotrek.monitoring.Adapters.SectionsPageAdapter;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Events;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Reminders;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab1;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab2;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab3;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab4;
import com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab5;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.R.attr.data;
import static android.R.id.toggle;

/**
 * Created by Piotrek on 13.01.2018.
 */

public class StatisticsMenu extends AppCompatActivity {

    private ViewPager mViewPager;


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_view);
        setTitle("Przeglądaj swoje dane");

        Intent intent = getIntent();
        String array = intent.getStringExtra("array");

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayoutStatistics);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.flContent3);
        mViewPager.setOffscreenPageLimit(7);
        setupViewPager(mViewPager);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


        final NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_menu3);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.nav_home): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(0);
                        setTitle("Przeglądaj swoje dane");
                        break;
                    }
                    case (R.id.nav_pulse): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(1);
                        setTitle("Tętno");
                        break;
                    }
                    case (R.id.nav_temp): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(2);
                        setTitle("Temperatura");
                        break;
                    }
                    case (R.id.nav_breath): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(3);
                        setTitle("Oddech");
                        break;
                    }
                    case (R.id.nav_pressure): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(4);
                        setTitle("Ciśnienie krwi");
                        break;
                    }
                    case (R.id.nav_steps): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(5);
                        setTitle("Kroki");
                        break;
                    }
                    case (R.id.nav_back): {
                        AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsMenu.this);
                        builder.setMessage("Czy na pewno chcesz wyjść?")
                                .setCancelable(false)
                                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        StatisticsMenu.this.finish();
                                    }
                                })
                                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;
                    }
                }
                return true;
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new WelcomeScreen(), "Przeglądaj swoje dane");
        adapter.addFragment(new PulseView(), "Graf tętna");
        adapter.addFragment(new TempView(), "Graf temperatury");
        adapter.addFragment(new BreathView(), "Graf oddechu");
        adapter.addFragment(new PressureView(), "Graf ciśnienia krwi");
        adapter.addFragment(new StepsView(), "Kroki w ciągu dnia");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        return toggle.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsMenu.this);
        builder.setMessage("Czy na pewno chcesz wyjść?")
                .setCancelable(false)
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StatisticsMenu.this.finish();
                    }
                })
                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
