package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.piotrek.monitoring.Data.Contact_data.UserData.UserProfile;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;
import com.google.gson.Gson;

import static com.example.piotrek.monitoring.R.id.etContactEmailUpdate;
import static com.example.piotrek.monitoring.R.id.etLogin;
import static com.example.piotrek.monitoring.R.id.etPassword;
import static java.security.AccessController.getContext;


/**
 * Created by Piotrek on 07.01.2018.
 */

public class FirstLoginInfo extends AppCompatActivity {

    private Button enter;
    private EditText etName, etSurname, etAge, etPesel, etEmail, etContact;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_login_info);

        final UserProfile userProfile = new UserProfile();

        final Intent intent = getIntent();

        id = intent.getStringExtra("id");

        enter = (Button) findViewById(R.id.btnEnter);

        etAge = (EditText) findViewById(R.id.firstAge);
        etEmail = (EditText) findViewById(R.id.firstEmail);
        etName = (EditText) findViewById(R.id.firstName);
        etSurname = (EditText) findViewById(R.id.firstSurname);
        etPesel = (EditText) findViewById(R.id.firstPesel);
        etContact = findViewById(R.id.contact_Email);

        etEmail.setText(intent.getStringExtra("email"));


        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addData = new Intent(getApplicationContext(), WaitInBackground.class);
                addData.putExtra("name", etName.getText().toString());
                addData.putExtra("surname", etSurname.getText().toString());
                addData.putExtra("age", etAge.getText().toString());
                addData.putExtra("pesel", etPesel.getText().toString());
                addData.putExtra("email", etEmail.getText().toString());
                addData.putExtra("id", id);
                addData.putExtra("firstTime", 0);
                addData.putExtra("contact", etContact.getText().toString());
                boolean requestFirstTime = true;
                addData.putExtra("firstLogin", requestFirstTime);
                startActivityForResult(addData, 1);
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(2);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(getApplicationContext(), UserAreaActivity.class);
                intent.putExtra("name", etName.getText().toString());
                intent.putExtra("surname", etSurname.getText().toString());
                intent.putExtra("age", etAge.getText().toString());
                intent.putExtra("pesel", etPesel.getText().toString());
                intent.putExtra("email", etEmail.getText().toString());
                intent.putExtra("user_id", id);
                intent.putExtra("contact", etContact.getText().toString());
                finish();
                startActivity(intent);
            } else if (resultCode == 2) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("Błąd dodawania danych.");
                builder.show();
                etName.setText("");
                etSurname.setText("");
                etAge.setText("");
                etPesel.setText("");
                etEmail.setText("");
                etContact.setText("");
            }
        }
    }
}
