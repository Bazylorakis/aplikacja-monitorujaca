package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.piotrek.monitoring.Adapters.NotificationDbAdapter;
import com.example.piotrek.monitoring.Adapters.NotificationTemplate;
import com.example.piotrek.monitoring.Adapters.StepsTemplate;
import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by Piotrek on 21.01.2018.
 */

public class StepsView extends android.support.v4.app.Fragment {


    int daySteps = 0;
    TextView tvSteps;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_steps_view, container, false);

         tvSteps =  view.findViewById(R.id.tvStepsInfo);

        Intent intent = getActivity().getIntent();
        String array = intent.getStringExtra("array");
        try {
            JSONArray jsonArray = new JSONArray(array);
            String data = jsonArray.getJSONObject(0).getString("date");
            String userId = jsonArray.getJSONObject(0).getString("user_id");
            final NotificationDbAdapter notificationDbAdapter = new NotificationDbAdapter(getContext());
            notificationDbAdapter.open();
            final Cursor dbCursor = notificationDbAdapter.getUserSteps(userId, data);
            if (dbCursor.moveToFirst()) {
                do {
                    try {
                        StepsTemplate template = notificationDbAdapter.getStepsReadingsFromCursor(dbCursor);
                        daySteps += template.mStepsCounter;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (dbCursor.moveToNext());
            }
            notificationDbAdapter.close();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(daySteps != 0){
            tvSteps.setText("Tego dnia przebyto: " + Integer.toString(daySteps) + " kroków.");
        }
        else{
            tvSteps.setText("Nie zebrano żadnych danych.");
        }






        return view;
    }
}
