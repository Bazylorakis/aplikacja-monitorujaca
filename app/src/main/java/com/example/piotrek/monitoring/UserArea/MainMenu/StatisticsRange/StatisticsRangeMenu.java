package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.example.piotrek.monitoring.Adapters.SectionsPageAdapter;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReading;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.BreathView;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.PressureView;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.PulseView;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.StatisticsMenu;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.StepsView;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.TempView;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.WelcomeScreen;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import android.support.v4.view.ViewPager;

import com.example.piotrek.monitoring.Adapters.SectionsPageAdapter;

/**
 * Created by Piotrek on 22.01.2018.
 */

public class StatisticsRangeMenu extends AppCompatActivity {

    private ViewPager mViewPager;


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    ArrayList<DayReadings> readings;
    ArrayList<Date> date;
    ArrayList<AvgClass> averages;
    DayReadings dayReadings;

    int sumPulse, sumBreath, sumSkurcz, sumRozkurcz;
    double sumTemps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_range_menu);
        setTitle("Przeglądaj swoje dane");

        readings = new ArrayList<>();
        date = new ArrayList<>();

        Intent intent = getIntent();
        String array = intent.getStringExtra("array");
        String begin = intent.getStringExtra("dateBegin");
        String end = intent.getStringExtra("dateEnd");
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            JSONArray jsonArray = new JSONArray(array);
            for (int i = 0; i < jsonArray.length(); i++) {
                Date currentDate = format.parse(jsonArray.getJSONObject(i).getString("date"));
                if (currentDate.after(format.parse(begin)) && currentDate.before(format.parse(end))) {
                    date.add(currentDate);
                }
            }

            Set<Date> hs = new LinkedHashSet<>(date);
            hs.addAll(date);
            date.clear();
            date.addAll(hs);

            for(int i = 0; i<date.size(); i++) {
                readings.add(new DayReadings(date.get(i)));
                for(int j=0; j<jsonArray.length(); j++) {
                    if (format.parse(jsonArray.getJSONObject(j).getString("date")).getTime()
                            ==  readings.get(i).mDateOfReadings.getTime()) {
                        readings.get(i).addVariables(readings.get(i),
                                Integer.parseInt(jsonArray.getJSONObject(j).getString("breath")),
                                Integer.parseInt(jsonArray.getJSONObject(j).getString("pulse")),
                                Double.parseDouble(jsonArray.getJSONObject(j).getString("temp")),
                                Integer.parseInt(jsonArray.getJSONObject(j).getString("skurcz")),
                                Integer.parseInt(jsonArray.getJSONObject(j).getString("rozkurcz"))
                        );
                    }
                }
            }

            averages = new ArrayList<>();
            for(int i = 0; i<readings.size(); i++){
                averages.add(new AvgClass(
                        readings.get(i).mDateOfReadings,
                        readings.get(i).avgPulse(readings.get(i)),
                        readings.get(i).avgBreath(readings.get(i)),
                        readings.get(i).avgSkurcz(readings.get(i)),
                        readings.get(i).avgRozkurcz(readings.get(i)),
                        readings.get(i).avgTemps(readings.get(i))
                ));
        }
        if(averages.size() == 0 || averages.size() == 1){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Za mało danych w okresie.");
            builder.create();
            builder.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();                }
            }, 1500);
        }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toolbar =  findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        drawerLayout =  findViewById(R.id.drawerLayoutStatisticsRange);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.flContent4);
        mViewPager.setOffscreenPageLimit(7);
        setupViewPager(mViewPager);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


        final NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_menu4);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.nav_home): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(0);
                        setTitle("Przeglądaj swoje dane");
                        break;
                    }
                    case (R.id.nav_pulse): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(1);
                        setTitle("Tętno");
                        break;
                    }
                    case (R.id.nav_temp): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(2);
                        setTitle("Temperatura");
                        break;
                    }
                    case (R.id.nav_breath): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(3);
                        setTitle("Oddech");
                        break;
                    }
                    case (R.id.nav_pressure): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(4);
                        setTitle("Ciśnienie krwi");
                        break;
                    }
                    case (R.id.nav_back): {
                        AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsRangeMenu.this);
                        builder.setMessage("Czy na pewno chcesz wyjść?")
                                .setCancelable(false)
                                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        StatisticsRangeMenu.this.finish();
                                    }
                                })
                                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;
                    }
                }
                return true;
            }
        });
    };


    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new WelcomeScreenLong(), "Przeglądaj swoje dane");
        adapter.addFragment(new PulseViewLong(), "Graf tętna");
        adapter.addFragment(new TempViewLong(), "Graf temperatury");
        adapter.addFragment(new BreathViewLong(), "Graf oddechu");
        adapter.addFragment(new PressureViewLong(), "Graf ciśnienia krwi");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        return toggle.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsRangeMenu.this);
        builder.setMessage("Czy na pewno chcesz wyjść?")
                .setCancelable(false)
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StatisticsRangeMenu.this.finish();
                    }
                })
                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}