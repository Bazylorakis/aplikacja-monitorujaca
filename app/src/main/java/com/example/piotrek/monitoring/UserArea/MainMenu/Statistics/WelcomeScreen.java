package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Piotrek on 13.01.2018.
 */

public class WelcomeScreen extends android.support.v4.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statistics_welcome_screen, container, false);

        TextView tvWelcomeInfo = (TextView) view.findViewById(R.id.tvStatInfo);


        Intent intent = getActivity().getIntent();

        String array = intent.getStringExtra("array");

        try {
            JSONArray jsonArray = new JSONArray(array);
            String date = jsonArray.getJSONObject(0).getString("date");
            tvWelcomeInfo.setText("Przeglądasz statystyki z całego dnia.\n" + date);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
