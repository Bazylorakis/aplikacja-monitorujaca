package com.example.piotrek.monitoring.MainApp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;


import static android.app.Activity.RESULT_OK;


/**
 * Created by Piotrek on 27.10.2017.
 */

public class Tab2Fragment extends Fragment {

    private static final String TAG = "Tab1Fragment";
    EditText etLogin;
    EditText etPassword;
    EditText etEmail;
    TextView back;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2_fragment, container, false);


        etLogin = view.findViewById(R.id.etLogin);
        etPassword = view.findViewById(R.id.etPassword);
        etEmail = view.findViewById(R.id.etEmail);
        back = view.findViewById(R.id.tvBack);


        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ViewPager vp = getActivity().findViewById(R.id.container);
                vp.setCurrentItem(0);
            }
        });


        final Button bRegister = view.findViewById(R.id.btnRegister);


        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_login = etLogin.getText().toString();
                String str_password = etPassword.getText().toString();
                String str_email = etEmail.getText().toString();
                Boolean requestRegister = true;
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                intent.putExtra("requestRegister", requestRegister);
                intent.putExtra("reg_login", str_login);
                intent.putExtra("reg_pass", str_password);
                intent.putExtra("email", str_email);
                startActivityForResult(intent, 3);

            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                ViewPager vp = getActivity().findViewById(R.id.container);
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Rejestracja zakończona sukcesem.");
                builder.show();
                vp.setCurrentItem(0);
            } else if (resultCode == 4) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Błąd rejestracji.");
                builder.show();
                etLogin.setText("");
                etPassword.setText("");
                etEmail.setText("");
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
