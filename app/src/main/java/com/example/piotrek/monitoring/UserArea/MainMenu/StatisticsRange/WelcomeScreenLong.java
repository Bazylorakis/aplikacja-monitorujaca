package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;

/**
 * Created by Piotrek on 22.01.2018.
 */

public class WelcomeScreenLong extends android.support.v4.app.Fragment {

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statistics_welcome_screen_long, container, false);

        TextView tvWelcomeInfo =  view.findViewById(R.id.tvStatInfo);

        Intent intent = getActivity().getIntent();
        intent.getStringExtra("dateBegin");
        intent.getStringExtra("dateEnd");

            tvWelcomeInfo.setText(
                    "Przeglądasz statystyki z okresu\nod: "
                            + intent.getStringExtra("dateBegin")
                            + "\ndo: "
                            + intent.getStringExtra("dateEnd")
                    );

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}