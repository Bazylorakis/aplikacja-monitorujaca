package com.example.piotrek.monitoring.UserInterface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.piotrek.monitoring.BackgroundTasks.AddContactAsync;
import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.R;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Piotrek on 11.12.2017.
 */

public class AddNumberPop extends Activity {

    EditText editTextName;
    EditText editTextSurname;
    EditText editTextNumber;
    String name;
    String surname;
    String number;
    ProgressBar progress;
    TextView tvOperation;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addnumberpop);

        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");

        editTextName = findViewById(R.id.editTextName);
        editTextSurname = findViewById(R.id.editTextSurname);
        editTextNumber = findViewById(R.id.editTextNumber);
        final Button addContact = findViewById(R.id.addContact);
        progress = findViewById(R.id.progressBarContact);
        progress.setVisibility(View.INVISIBLE);
        tvOperation = findViewById(R.id.textViewProgress);
        tvOperation.setVisibility(View.INVISIBLE);

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        final int width = displayMetrics.widthPixels;
        final int height = displayMetrics.heightPixels;

        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = editTextName.getText().toString();
                surname = editTextSurname.getText().toString();
                number = editTextNumber.getText().toString();

                Intent intent = new Intent(getApplicationContext(), WaitInBackground.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("number", number);
                boolean requestAddNumber = true;
                intent.putExtra("requestAddNumber", requestAddNumber);
                startActivityForResult(intent, 1);
                ScrollView mLayout = (ScrollView) findViewById(R.id.scrollViewPop);
                mLayout.setVisibility(View.INVISIBLE);
            }
        });


        getWindow().setLayout((int) (width * 0.8), (int) (height * 0.5));

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("name", name);
        savedInstanceState.putString("surname", surname);
        savedInstanceState.putString("number", number);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("number", number);
                setResult(RESULT_OK, intent);
                finish();
            } else if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }
    }
}
