package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 22.01.2018.
 */

public class DayReadings {

    Date mDateOfReadings;
    ArrayList<Integer> mBreath;
    ArrayList<Integer> mPulse;
    ArrayList<Integer> mSkurcz;
    ArrayList<Integer> mRozkurcz;
    ArrayList<Double> mTemps;

    public DayReadings(Date date){
        this.mDateOfReadings = date;
        mBreath = new ArrayList<>();
        mPulse = new ArrayList<>();
        mSkurcz = new ArrayList<>();
        mRozkurcz = new ArrayList<>();
        mTemps = new ArrayList<>();
    }


    public void addVariables(DayReadings day, int breath, int pulse, double temp, int skurcz, int rozkurcz){
        day.mBreath.add(breath);
        day.mPulse.add(pulse);
        day.mTemps.add(temp);
        day.mSkurcz.add(skurcz);
        day.mRozkurcz.add(rozkurcz);
    }

    public int avgPulse(DayReadings day){
        int sum = 0;
        for(int i = 0; i<day.mPulse.size(); i++){
            sum += day.mPulse.get(i);
        }
        return sum/day.mPulse.size();
    }
    public int avgBreath(DayReadings day){
        int sum = 0;
        for(int i = 0; i<day.mBreath.size(); i++){
            sum += day.mBreath.get(i);
        }
        return sum/day.mBreath.size();
    }
    public int avgSkurcz(DayReadings day){
        int sum = 0;
        for(int i = 0; i<day.mSkurcz.size(); i++){
            sum += day.mSkurcz.get(i);
        }
        return sum/day.mSkurcz.size();
    }
    public int avgRozkurcz(DayReadings day){
        int sum = 0;
        for(int i = 0; i<day.mRozkurcz.size(); i++){
            sum += day.mRozkurcz.get(i);
        }
        return sum/day.mRozkurcz.size();
    }
    public double avgTemps(DayReadings day){
        int sum = 0;
        for(int i = 0; i<day.mTemps.size(); i++)
            sum += day.mTemps.get(i);
        return sum/day.mTemps.size();
    }

}
