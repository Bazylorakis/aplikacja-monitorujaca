package com.example.piotrek.monitoring.UserArea.MainMenu.Day;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.piotrek.monitoring.Data.Contact_data.Event;
import com.example.piotrek.monitoring.Data.Contact_data.User_events;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import static android.R.attr.name;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by Piotrek on 06.01.2018.
 */

public class Day extends android.support.v4.app.Fragment {

    EditText eventDesc, eventName;
    TimePicker tp;
    Button btnAdd;
    private String user_id;
    private TextView tvSigns1, tvSigns2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.day_fragment, container, false);

        eventDesc = view.findViewById(R.id.etEventDesc);
        eventName = view.findViewById(R.id.etEventName);
        btnAdd = view.findViewById(R.id.btnAddEvent);

        tvSigns1 = view.findViewById(R.id.tvSigns1);
        tvSigns2 = view.findViewById(R.id.tvSigns2);

        GetDayData getDayData = (GetDayData) getActivity();

        user_id = getDayData.getData().get(0);


        tp = view.findViewById(R.id.tpEventTime);
        tp.setIs24HourView(true);


        eventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int left = 32;
                int length = eventName.length();
                tvSigns1.setText("Pozostałe znaki: " + Integer.toString(left - length));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        eventDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int left = 255;
                int length = eventName.length();
                tvSigns2.setText("Pozostałe znaki: " + Integer.toString(left - length));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String h;
                String m;
                int hour = tp.getCurrentHour();
                int minute = tp.getCurrentMinute();
                if (minute < 10)
                    m = "0" + Integer.toString(minute);
                else
                    m = Integer.toString(minute);
                h = Integer.toString(hour);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy " + h + ":" + m);
                String eventDate = df.format(c.getTime());
                Log.d("DATA WYDARZENIA: ", eventDate);
                String eventN = eventName.getText().toString();
                String eventD = eventDesc.getText().toString();
                if (!eventN.matches("") && !eventD.matches("")) {
                    Intent intent = new Intent(getContext(), WaitInBackground.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("eventDate", eventDate);
                    intent.putExtra("eventName", eventN);
                    intent.putExtra("eventDesc", eventD);
                    boolean requestAddEvent = true;
                    intent.putExtra("requestAddEvent", requestAddEvent);
                    startActivityForResult(intent, 1);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Wypełnij wszystkie pola!");
                    builder.create();
                    builder.show();
                }
            }
        });


        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Dodano pomyślnie!");
                builder.create();
                builder.show();
                getActivity().setResult(RESULT_OK);
            } else if (resultCode == RESULT_CANCELED) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setMessage("Wystąpił błąd!");
                builder.create().show();
            }
        }
    }

}
