package com.example.piotrek.monitoring.Adapters;

import com.example.piotrek.monitoring.Data.Contact_data.BasicReading;

import java.util.Comparator;

/**
 * Created by Piotrek on 20.01.2018.
 */

public class CustomComparator implements Comparator<BasicReading> {// may be it would be Model
    @Override
    public int compare(BasicReading obj1, BasicReading obj2) {
        return obj1.getDate().compareTo(obj2.getDate());// compare two objects
    }
}