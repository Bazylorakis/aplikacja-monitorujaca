package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.date.DayPickerView;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReadingAdapter;
import com.example.piotrek.monitoring.MainApp.MainActivity;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.StatisticsMenu;
import com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange.StatisticsRangeMenu;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.DoubleDateAndTimePickerDialog;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Piotrek on 22.01.2018.
 */

public class StatsFromPeriod extends android.support.v4.app.Fragment implements DatePickerDialog.OnDateSetListener {

    String TAG = "Statystyki okresowe";

    Button btnGetRange;

    String start = "";
    String stop = "";

    String user_id;
    String dateBegin, dateEnd;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats_period, container, false);

        UserAreaActivity userAreaActivity = (UserAreaActivity) getActivity();

        user_id = userAreaActivity.getData().get(0);

        btnGetRange = view.findViewById(R.id.buttonPickRange);

        btnGetRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        StatsFromPeriod.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "Statystyki okresowe");
            }
        });

        return view;
    }


    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        if (dayOfMonth >= 10 && monthOfYear >= 10) {
             dateBegin = dayOfMonth +"-"+(++monthOfYear)+"-"+year;
        } else if (dayOfMonth >= 10 && monthOfYear < 10) {
             dateBegin = dayOfMonth + "-" + "0" + (++monthOfYear) + "-" + year;
        } else if(dayOfMonth<10 && monthOfYear >= 10)
        {
            dateBegin = "0" + dayOfMonth + "-" + (++monthOfYear) + "-" + year;
        }
        else {
             dateBegin = "0" + dayOfMonth + "-" + "0" + (++monthOfYear) + "-" + year;
        }

        if (dayOfMonthEnd >= 10 && monthOfYearEnd >= 10) {
             dateEnd = +dayOfMonthEnd+"-"+(++monthOfYearEnd)+"-"+yearEnd;
        } else if (dayOfMonthEnd >= 10 && monthOfYearEnd < 10) {
             dateEnd = dayOfMonthEnd + "-" + "0" + (++monthOfYearEnd) + "-" + yearEnd;
        } else if(dayOfMonthEnd<10 && monthOfYearEnd >= 10)
        {
            dateBegin = "0" + dayOfMonthEnd + "-" + (++monthOfYearEnd) + "-" + yearEnd;
        }else {
             dateEnd = "0" + dayOfMonthEnd + "-" + "0" + (++monthOfYearEnd) + "-" + yearEnd;
        }

        if(!dateBegin.equals("") && !dateEnd.equals(""))
        {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date begin = format.parse(dateBegin);
                Date stop = format.parse(dateEnd);
                if(begin.after(stop) || stop.before(begin)){
                    Log.d("DATY", begin.toString() + " " + stop.toString());
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Wybierz odpowiednie daty!");
                    builder.create();
                    builder.show();
                }
                else if(begin.equals(stop)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Daty z pojedynczego dnia obsługuje inna funkcjonalność!");
                    builder.create();
                    builder.show();
                }
                else{
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                boolean requestDataRange = true;
                intent.putExtra("user_id", user_id);
                intent.putExtra("requestDataRange", requestDataRange);
                startActivityForResult(intent, 1);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Coś poszło nie tak.");
            builder.create();
            builder.show();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jsonObject = new JSONObject(data.getStringExtra("output"));
                    Log.d("Output po kliknieciu:", data.getStringExtra("output"));
                    Boolean response = jsonObject.getBoolean("success_getDataRange");
                    if (response) {
                        JSONArray arr = jsonObject.getJSONArray("basics");
                        String array = arr.toString();
                        Intent intent = new Intent(getContext(), StatisticsRangeMenu.class);
                        intent.putExtra("array", array);
                        intent.putExtra("dateBegin", dateBegin);
                        intent.putExtra("dateEnd", dateEnd);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(getContext(), "Statystyki są puste!", Toast.LENGTH_SHORT).show();
        }
    }

}
