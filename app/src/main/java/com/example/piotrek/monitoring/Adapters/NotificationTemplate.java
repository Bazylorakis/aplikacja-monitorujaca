package com.example.piotrek.monitoring.Adapters;

import com.example.piotrek.monitoring.Data.Contact_data.UserNotification;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 14.01.2018.
 */

public class NotificationTemplate {

    public int mNotificationId;
    public String mUserId;
    public String mTitle;
    public String mTime_of_notification;
    public int mRandomId;


    public NotificationTemplate() {
    }

    public NotificationTemplate(String userId, String title, String date, int randomId) {
        this.mUserId = userId;
        this.mTitle = title;
        this.mTime_of_notification = date;
        this.mRandomId = randomId;
    }

    public static ArrayList<NotificationTemplate> makeNotification() {
        ArrayList<NotificationTemplate> notify = new ArrayList<>();
        return notify;
    }


    public ArrayList<NotificationTemplate> getNotifications() {
        ArrayList<NotificationTemplate> notifications = new ArrayList<>();
        return notifications;
    }

}
