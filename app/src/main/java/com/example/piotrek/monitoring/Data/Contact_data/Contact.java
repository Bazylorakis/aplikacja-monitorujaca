package com.example.piotrek.monitoring.Data.Contact_data;

import java.util.ArrayList;

/**
 * Created by Piotrek on 15.12.2017.
 */

public class Contact {
    public String mName;
    public String mSurname;
    public String mNumber;

    public Contact() {
    }

    public Contact(String name, String surname, String number) {
        this.mName = name;
        this.mSurname = surname;
        this.mNumber = number;
    }

    public Contact newContact(String name, String surname, String number) {
        Contact contact = new Contact(name, surname, number);
        return contact;
    }

    public ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();
        return contacts;
    }
}
