package com.example.piotrek.monitoring.Interfaces;

/**
 * Created by Piotrek on 08.12.2017.
 */

public interface AsyncResponse {
    void processFinish(String output);

    void processStart(String input);
}
