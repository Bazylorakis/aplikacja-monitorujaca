package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static android.media.CamcorderProfile.get;
import static com.example.piotrek.monitoring.R.id.container;

/**
 * Created by Piotrek on 13.01.2018.
 */

public class PulseView extends android.support.v4.app.Fragment {

    ArrayList<Integer> pulse;
    ArrayList<Double> time;

    GraphView graphView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pulse_view, container, false);

        graphView = view.findViewById(R.id.pulseGraph);


        Intent intent = getActivity().getIntent();
        String array = intent.getStringExtra("array");
        pulse = new ArrayList<>();
        time = new ArrayList<>();

        try {
            LineGraphSeries<DataPoint> series;
            JSONArray jsonArray = new JSONArray(array);
            for (int i = 0; i < jsonArray.length(); i++) {
                if (!jsonArray.getJSONObject(i).getString("pulse").matches(""))
                    pulse.add(Integer.parseInt(jsonArray.getJSONObject(i).getString("pulse")));
                if (!jsonArray.getJSONObject(i).getString("time_of_reading").matches("")) {
                    String toReplace = jsonArray.getJSONObject(i).getString("time_of_reading");
                    char[] replace = toReplace.toCharArray();
                    replace[2] = '.';
                    toReplace = String.valueOf(replace);
                    time.add(Double.parseDouble(toReplace));
                }
            }

            int size = pulse.size();
            DataPoint[] values = new DataPoint[size];
            for (int i = 0; i < size; i++) {
                Double xi = time.get(i);
                Integer yi = pulse.get(i);
                DataPoint v = new DataPoint(xi, yi);
                values[i] = v;
            }

            series = new LineGraphSeries<DataPoint>(values);
            graphView.addSeries(series);
            series.setDrawDataPoints(true);
            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMinY(30);
            graphView.getViewport().setMaxY(230);
            graphView.getViewport().setXAxisBoundsManual(true);
            graphView.getViewport().setMinX(0);
            graphView.getViewport().setMaxX(24);
            graphView.getGridLabelRenderer().setNumHorizontalLabels(24);
            graphView.getGridLabelRenderer().setHumanRounding(true);
            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getContext(), "Tętno.\nGodzina: " + dataPoint.getX() +"\nWartość: "+ dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);}

}
