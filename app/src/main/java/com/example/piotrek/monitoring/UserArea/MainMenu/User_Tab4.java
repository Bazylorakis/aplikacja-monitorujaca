package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.ContentValues;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.piotrek.monitoring.Adapters.AndroidDatabaseManager;
import com.example.piotrek.monitoring.Adapters.NotificationDbAdapter;
import com.example.piotrek.monitoring.Adapters.StepDetector;
import com.example.piotrek.monitoring.Adapters.StepListener;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;


import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by Piotrek on 03.01.2018.
 */


public class User_Tab4 extends Fragment implements SensorEventListener, StepListener{

    private static final String TAG = "UserTab1Fragment";
    private TextView TvSteps;
    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Liczba kroków: ";
    private int numSteps;
    private Button BtnStart, BtnStop;
    String user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_tab4, container, false);

        UserAreaActivity userAreaActivity = (UserAreaActivity) getActivity();
        user_id = userAreaActivity.getData().get(0);

        Button button = (Button) view.findViewById(R.id.database);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AndroidDatabaseManager.class);
                startActivity(intent);
            }
        });

        sensorManager = (SensorManager) getContext().getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);

        TvSteps =  view.findViewById(R.id.tv_steps);
        BtnStart =  view.findViewById(R.id.btn_start);
        BtnStop =  view.findViewById(R.id.btn_stop);


        BtnStart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                numSteps = 0;
                sensorManager.registerListener(User_Tab4.this, accel, SensorManager.SENSOR_DELAY_FASTEST);

            }
        });


        BtnStop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                sensorManager.unregisterListener(User_Tab4.this);
                long time = System.currentTimeMillis();
                Date date = new Date(time);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String d = sdf.format(date);
                Log.d("CURRENTTIME", Long.toString(time));

                NotificationDbAdapter stepsDbAdapter = new NotificationDbAdapter(getContext());
                stepsDbAdapter.open();
                ContentValues newValues = new ContentValues();
                newValues.put(stepsDbAdapter.USER_ID, user_id);
                newValues.put(stepsDbAdapter.DATE, d);
                newValues.put(stepsDbAdapter.STEPS, numSteps);
                stepsDbAdapter.insertSteps(newValues);
                stepsDbAdapter.close();
            }
        });
        return view;
    }


    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }
    public void step(long timeNs) {
        numSteps++;
        TvSteps.setText(TEXT_NUM_STEPS + numSteps);
    }



    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}
