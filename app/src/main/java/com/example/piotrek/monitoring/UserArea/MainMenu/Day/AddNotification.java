package com.example.piotrek.monitoring.UserArea.MainMenu.Day;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.piotrek.monitoring.Adapters.NotificationDbAdapter;
import com.example.piotrek.monitoring.Adapters.NotificationTemplate;
import com.example.piotrek.monitoring.MainApp.MainActivity;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;

import junit.framework.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Piotrek on 10.01.2018.
 */

public class AddNotification extends android.support.v4.app.Fragment {

    EditText etNotificationName;
    TextView tvCounter;
    Button btnAddNotification;
    TimePicker tp;
    ArrayList<NotificationTemplate> notifications;
    Switch repeat;
    Random generator;
    String currentDate;
    String CHANNEL_NAME = "My channel";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment, container, false);
        tp = view.findViewById(R.id.tpNotificationTime);
        tp.setIs24HourView(true);

        notifications = new ArrayList<>();

        tvCounter = view.findViewById(R.id.tvSigns3);
        etNotificationName = view.findViewById(R.id.etNotificationName);
        btnAddNotification = view.findViewById(R.id.btnAddNotification);
        Intent intent = getActivity().getIntent();

        currentDate = intent.getStringExtra("date");
        final String user_id = intent.getStringExtra("id");

        generator = new Random();

        if(Build.VERSION.SDK_INT>=26) {
            NotificationManager manager = (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
            final String id = "My_channel_01";
            CharSequence name = "Channel_name";
            String desc = "Mobile_app_channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            mChannel.setDescription(desc);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            manager.createNotificationChannel(mChannel);

            btnAddNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        int notiId = generator.nextInt();
                        scheduleNotification(getContext(), notiId, id);
                        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00"));
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        if(Build.VERSION.SDK_INT>=23) {
                            calendar.set(Calendar.HOUR_OF_DAY, tp.getHour());
                            calendar.set(Calendar.MINUTE, tp.getMinute());
                            SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");
                            String formatted = format1.format(calendar.getTime());

                            NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
                            adapter.open();
                            ContentValues newValues = new ContentValues();
                            newValues.put(adapter.RANDOM_ID, notiId);
                            newValues.put(adapter.TIME_OF_NOTIFICATION, formatted);
                            newValues.put(adapter.NOTIFICATION_TITLE, etNotificationName.getText().toString());
                            newValues.put(adapter.USER_ID, user_id);
                            adapter.insertNotification(newValues);
                            adapter.close();
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                            dialog.setMessage("Dodano pomyślnie");
                                   dialog.create();
                                   dialog.show();
                        }
                        else{
                            calendar.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
                            calendar.set(Calendar.MINUTE, tp.getCurrentMinute());

                            SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");
                            String formatted = format1.format(calendar.getTime());


                            NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
                            adapter.open();
                            ContentValues newValues = new ContentValues();
                            newValues.put(adapter.RANDOM_ID, notiId);
                            newValues.put(adapter.TIME_OF_NOTIFICATION, formatted);
                            newValues.put(adapter.NOTIFICATION_TITLE, etNotificationName.getText().toString());
                            newValues.put(adapter.USER_ID, user_id);
                            adapter.insertNotification(newValues);
                            adapter.close();

                            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                            dialog.setMessage("Dodano pomyślnie");
                            dialog.create();
                            dialog.show();
                        }
            }});
        }
        else{
            btnAddNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        final String id = "My_channel_01";
                        int notiId = generator.nextInt();
                        scheduleNotification(getContext(), notiId, id);
                        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00"));
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        if(Build.VERSION.SDK_INT>=23) {
                            calendar.set(Calendar.HOUR_OF_DAY, tp.getHour());
                            calendar.set(Calendar.MINUTE, tp.getMinute());
                            SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");
                            String formatted = format1.format(calendar.getTime());
                            NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
                            adapter.open();
                            ContentValues newValues = new ContentValues();
                            newValues.put(adapter.RANDOM_ID, notiId);
                            newValues.put(adapter.TIME_OF_NOTIFICATION, formatted);
                            newValues.put(adapter.NOTIFICATION_TITLE, etNotificationName.getText().toString());
                            newValues.put(adapter.USER_ID, user_id);
                            adapter.insertNotification(newValues);
                            adapter.close();
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                            dialog.setMessage("Dodano pomyślnie");
                            dialog.create();
                            dialog.show();
                        }
                        else{
                            calendar.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
                            calendar.set(Calendar.MINUTE, tp.getCurrentMinute());
                            SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");
                            String formatted = format1.format(calendar.getTime());

                            NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
                            adapter.open();
                            ContentValues newValues = new ContentValues();
                            newValues.put(adapter.RANDOM_ID, notiId);
                            newValues.put(adapter.TIME_OF_NOTIFICATION, formatted);
                            newValues.put(adapter.NOTIFICATION_TITLE, etNotificationName.getText().toString());
                            newValues.put(adapter.USER_ID, user_id);
                            adapter.insertNotification(newValues);
                            adapter.close();
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                            dialog.setMessage("Dodano pomyślnie");
                            dialog.create();
                            dialog.show();
                        }
                }
            });
        }


        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(64);
        etNotificationName.setFilters(filterArray);

        etNotificationName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int left = 64;
                int length = etNotificationName.length();
                tvCounter.setText("Pozostałe znaki: " + Integer.toString(left - length));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void scheduleNotification(Context context, int notificationId, String channelId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setContentTitle("Aplikacja medyczna")
                .setContentText(etNotificationName.getText().toString())
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_cardiogram)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);
        Notification notification = builder.build();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00"));
        calendar.setTimeInMillis(System.currentTimeMillis());

        if(Build.VERSION.SDK_INT>=23) {
            calendar.set(Calendar.HOUR_OF_DAY, tp.getHour());
            calendar.set(Calendar.MINUTE, tp.getMinute());
        }
        else{
            calendar.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
            calendar.set(Calendar.MINUTE, tp.getCurrentMinute());
        }
        Intent notificationIntent = new Intent(context, Receiver.class);
        notificationIntent.putExtra(Receiver.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(Receiver.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}

