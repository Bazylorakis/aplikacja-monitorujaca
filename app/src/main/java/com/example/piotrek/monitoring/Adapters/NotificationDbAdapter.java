package com.example.piotrek.monitoring.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Piotrek on 14.01.2018.
 */

public class NotificationDbAdapter {
    private static final String DATABASE_NAME = "USER_NOTIFICATIONS.db";
    private static final String NOTIFICATIONS_TABLE = "NOTIFICATIONS_TABLE";
    private static final String USER_STEPS_TABLE = "USER_STEPS";
    private static final int DATABASE_VERSION = 200;
    private final Context mContext;
    public static String TAG = NotificationDbAdapter.class.getSimpleName();
    private static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    public DatabaseHelper mDbHelper;
    SQLiteDatabase mDb;

    public static final String KEY_ROWID = "_id";
    public static final String USER_ID = "user_id";
    public static final String RANDOM_ID = "random_id";
    public static final String TIME_OF_NOTIFICATION = "time_of_notification";
    public static final String NOTIFICATION_TITLE = "title";

    public static final String STEPS = "random_id";
    public static final String DATE = "time_of_notification";

    public static final String[] NOTIFICATION_FIELDS = new String[]{
            KEY_ROWID,
            USER_ID,
            RANDOM_ID,
            TIME_OF_NOTIFICATION,
            NOTIFICATION_TITLE
    };


    public static final String[] STEPS_FIELDS = new String[]{
            KEY_ROWID,
            USER_ID,
            STEPS,
            DATE
    };

    private static final String CREATE_NOTIFICATION_TABLE =
            "create table " + NOTIFICATIONS_TABLE + "("
                    + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + USER_ID + " text,"
                    + RANDOM_ID + " INTEGER,"
                    + TIME_OF_NOTIFICATION + " text,"
                    + NOTIFICATION_TITLE + " text"
                    + ");";

    private static final String CREATE_STEPS_TABLE =
            "create table " + USER_STEPS_TABLE + "("
                    + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + USER_ID + " text,"
                    + STEPS + " INTEGER,"
                    + DATE + " LONG);";


    public static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public ArrayList<Cursor> getData(String Query) {
            //get writable database
            SQLiteDatabase sqlDB = this.getWritableDatabase();
            String[] columns = new String[]{"message"};
            //an array list of cursor to save two cursors one has results from the query
            //other cursor stores error message if any errors are triggered
            ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
            MatrixCursor Cursor2 = new MatrixCursor(columns);
            alc.add(null);
            alc.add(null);

            try {
                String maxQuery = Query;
                //execute the query results will be save in Cursor c
                Cursor c = sqlDB.rawQuery(maxQuery, null);

                //add value to cursor2
                Cursor2.addRow(new Object[]{"Success"});

                alc.set(1, Cursor2);
                if (null != c && c.getCount() > 0) {

                    alc.set(0, c);
                    c.moveToFirst();

                    return alc;
                }
                return alc;
            } catch (SQLException sqlEx) {
                Log.d("printing exception", sqlEx.getMessage());
                //if any exceptions are triggered save the error message to cursor an return the arraylist
                Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
                alc.set(1, Cursor2);
                return alc;
            } catch (Exception ex) {
                Log.d("printing exception", ex.getMessage());

                //if any exceptions are triggered save the error message to cursor an return the arraylist
                Cursor2.addRow(new Object[]{"" + ex.getMessage()});
                alc.set(1, Cursor2);
                return alc;
            }
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_NOTIFICATION_TABLE);
            db.execSQL(CREATE_STEPS_TABLE);
            Log.d("Insert:", "Sukces");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Aktualizacja bazy danych z wersji " + oldVersion + " na wersje " + newVersion + ", co spowoduje usunięcie wszystkich danych z bazy!");
            db.execSQL("DROP TABLE IF EXISTS " + NOTIFICATIONS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + USER_STEPS_TABLE);
            onCreate(db);
        }
    }

    public NotificationDbAdapter(Context ctx) {
        this.mContext = ctx;
    }

    public NotificationDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public NotificationDbAdapter delete() {
        mDbHelper = new DatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        mDb.execSQL("DROP TABLE IF EXISTS " + NOTIFICATIONS_TABLE);
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public void upgrade() throws SQLException {
        mDbHelper = new DatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        mDbHelper.onUpgrade(mDb, 1, 0);
    }


    public long insertNotification(ContentValues initialValues) {
        Log.d("Insert:", "Sukces");
        return mDb.insertWithOnConflict(NOTIFICATIONS_TABLE, null, initialValues, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public boolean updateNotification(int id, ContentValues newValues) {
        String[] selectionArgs = {String.valueOf(id)};
        return mDb.update(NOTIFICATIONS_TABLE, newValues, KEY_ROWID + "=?", selectionArgs) > 0;
    }

    public boolean deleteNotification(String id, String title) {
        String[] selectionArgs = {id, title};
        return mDb.delete(NOTIFICATIONS_TABLE, USER_ID + "=?" + " AND " + NOTIFICATION_TITLE + "=?", selectionArgs) > 0;
    }

    public Cursor getNotifications() {
        return mDb.query(NOTIFICATIONS_TABLE, NOTIFICATION_FIELDS, null, null, null, null, null, null);
    }

    public Cursor getUserNotifications(String id) {
        return mDb.query(NOTIFICATIONS_TABLE, NOTIFICATION_FIELDS, USER_ID + "=?", new String[]{id}, null, null, null);
    }


    public static NotificationTemplate getNotificationFromCursor(Cursor cursor) throws ParseException {
        NotificationTemplate notificationTemplate = new NotificationTemplate();
        notificationTemplate.mNotificationId = cursor.getInt(cursor.getColumnIndex(KEY_ROWID));
        notificationTemplate.mUserId = cursor.getString(cursor.getColumnIndex(USER_ID));
        notificationTemplate.mTitle = cursor.getString(cursor.getColumnIndex(NOTIFICATION_TITLE));
        notificationTemplate.mTime_of_notification = cursor.getString(cursor.getColumnIndex(TIME_OF_NOTIFICATION));
        notificationTemplate.mRandomId = cursor.getInt(cursor.getColumnIndex(RANDOM_ID));
        return notificationTemplate;
    }


    public long insertSteps(ContentValues initialValues) {
        return mDb.insertWithOnConflict(USER_STEPS_TABLE, null, initialValues, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public boolean updateSteps(int id, ContentValues newValues) {
        String[] selectionArgs = {String.valueOf(id)};
        return mDb.update(USER_STEPS_TABLE, newValues, KEY_ROWID + "=?", selectionArgs) > 0;
    }

    public boolean deleteSteps(String id, String title) {
        String[] selectionArgs = {id, title};
        return mDb.delete(USER_STEPS_TABLE, USER_ID + "=?" + " AND " + USER_STEPS_TABLE + "=?", selectionArgs) > 0;
    }

    public Cursor getAllSteps() {
        return mDb.query(USER_STEPS_TABLE, STEPS_FIELDS, null, null, null, null, null, null);
    }

    public Cursor getUserSteps(String id, String date) {
        return mDb.query(USER_STEPS_TABLE, STEPS_FIELDS, USER_ID + "=?" + " AND " + DATE + " =?", new String[]{id, date}, null, null, null);
    }


    public static StepsTemplate getStepsReadingsFromCursor(Cursor cursor) throws ParseException {
        StepsTemplate stepsTemplate = new StepsTemplate();
        stepsTemplate.mReadingId = cursor.getInt(cursor.getColumnIndex(KEY_ROWID));
        stepsTemplate.mUserId = cursor.getString(cursor.getColumnIndex(USER_ID));
        stepsTemplate.mStepsCounter = cursor.getInt(cursor.getColumnIndex(STEPS));
        stepsTemplate.mStepsDate = cursor.getString(cursor.getColumnIndex(DATE));
        return stepsTemplate;
    }

}
