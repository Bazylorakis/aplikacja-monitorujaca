package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.piotrek.monitoring.Adapters.CustomComparator;
import com.example.piotrek.monitoring.BackgroundTasks.GetBasicReadingsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveBasicReadingAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveEventAsync;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReading;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReadingAdapter;
import com.example.piotrek.monitoring.Data.Contact_data.User_events;
import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.Statistics.StatisticsMenu;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.example.piotrek.monitoring.R.id.textView;
import static com.example.piotrek.monitoring.R.id.tvLogin;

/**
 * Created by Piotrek on 27.10.2017.
 */

public class User_Tab2 extends Fragment implements AsyncResponse {

    ListView listView;
    ArrayList<BasicReading> readings;
    BasicReadingAdapter basicReadingAdapter;
    String user_id;
    BasicReading basicReading;

    FloatingActionButton fabRefresh;

    static GetBasicReadingsAsync getBasicReadingsAsync;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_tab2, container, false);


        getBasicReadingsAsync = new GetBasicReadingsAsync(getContext(), "getReadings");

        fabRefresh = view.findViewById(R.id.fabRefreshReadings);

        UserAreaActivity activity = (UserAreaActivity) getActivity();

        user_id = activity.getData().get(0);

        getBasicReadingsAsync.delegate = User_Tab2.this;
        try {
            getBasicReadingsAsync.execute(user_id);

        } catch (Exception e) {
            Log.d("Exception:", e.toString());
        }

        listView = view.findViewById(R.id.lvReadings);
        basicReading = new BasicReading();
        readings = basicReading.getReadings();
        basicReadingAdapter = new BasicReadingAdapter(getContext(), readings, user_id);
        listView.setAdapter(basicReadingAdapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                BasicReading currentReading = readings.get(i);
                String date = currentReading.mAcutalDate;
                intent.putExtra("date", date);
                intent.putExtra("id", user_id);
                boolean requestDayData = true;
                intent.putExtra("requestDayData", requestDayData);
                startActivityForResult(intent, 2);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int pos = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Czy na pewno chcesz usunąć wpis?")
                        .setCancelable(false)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                RemoveBasicReadingAsync removeBasicReadingAsync = new RemoveBasicReadingAsync(getContext(), "removeReading");
                                BasicReading currentReading = readings.get(pos);
                                String date = currentReading.mAcutalDate;
                                removeBasicReadingAsync.delegate = User_Tab2.this;
                                removeBasicReadingAsync.execute(user_id, date);
                                readings.remove(pos);
                                listView.setAdapter(basicReadingAdapter);
                                basicReadingAdapter.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });

        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readings.clear();
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                intent.putExtra("user_id", user_id);
                boolean requestRefreshReadings = true;
                intent.putExtra("requestRefreshReadings", requestRefreshReadings);
                startActivityForResult(intent, 1);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void processStart(String output) {

    }

    @Override
    public void processFinish(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getBasics");
            Log.d("Result w process:", output);
            if (response) {
                JSONArray arr = jsonObject.getJSONArray("basics");
                for (int i = 0; i < arr.length(); i++) {
                    String date = arr.getJSONObject(i).getString("date");
                    readings.add(basicReading.newReading(date));
                }
                Collections.sort(readings);
                BasicReadingAdapter basicReadingAdapter = new BasicReadingAdapter(getContext(), readings, user_id);
                listView.setAdapter(basicReadingAdapter);
                basicReadingAdapter.notifyDataSetChanged();
                listView.invalidateViews();
            } else {
                BasicReadingAdapter basicReadingAdapter = new BasicReadingAdapter(getContext(), readings, user_id);
                listView.setAdapter(basicReadingAdapter);
                basicReadingAdapter.notifyDataSetChanged();
                listView.invalidateViews();
                Toast.makeText(getContext(), "Statystyki są puste.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("removedReading");
            if (response) {
                Toast.makeText(getContext(), "Usunięto pomyślnie.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jsonObject = new JSONObject(data.getStringExtra("output"));
                    Boolean response = jsonObject.getBoolean("success_getBasics");
                    if (response) {
                        JSONArray arr = jsonObject.getJSONArray("basics");
                        for (int i = 0; i < arr.length(); i++) {
                            String date = arr.getJSONObject(i).getString("date");
                            readings.add(basicReading.newReading(date));
                        }
                        Collections.sort(readings);
                        BasicReadingAdapter basicReadingAdapter = new BasicReadingAdapter(getContext(), readings, user_id);
                        listView.setAdapter(basicReadingAdapter);
                        basicReadingAdapter.notifyDataSetChanged();
                        listView.invalidateViews();
                        Toast.makeText(getContext(), "Odświeżono!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(getContext(), "Statystyki są puste!", Toast.LENGTH_SHORT).show();
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jsonObject = new JSONObject(data.getStringExtra("output"));
                    Log.d("Output po kliknieciu:", data.getStringExtra("output"));
                    Boolean response = jsonObject.getBoolean("success_getStatistics");
                    if (response) {
                        JSONArray arr = jsonObject.getJSONArray("basics");
                        String array = arr.toString();
                        Intent intent = new Intent(getContext(), StatisticsMenu.class);
                        intent.putExtra("array", array);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(getContext(), "Nie udało się pobrać danych!", Toast.LENGTH_LONG).show();
        }


    }


}
