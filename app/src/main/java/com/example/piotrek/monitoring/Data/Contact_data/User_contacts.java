package com.example.piotrek.monitoring.Data.Contact_data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import java.util.ArrayList;

/**
 * Created by Piotrek on 10.12.2017.
 */

public class User_contacts extends BaseAdapter {
    Context mContext;
    ArrayList<Contact> mContacts;
    LayoutInflater layoutInflater;
    String mUser_id;

    public User_contacts(Context c, ArrayList<Contact> contacts, String user_id) {
        mUser_id = user_id;
        mContext = c;
        mContacts = contacts;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mContacts.size();
    }

    public Object getItem(int position) {
        return mContacts.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.contact_view, null);
            viewHolder = new ViewHolder();
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            viewHolder.textViewSurname = (TextView) convertView.findViewById(R.id.textViewSurname);
            viewHolder.textViewNumber = (TextView) convertView.findViewById(R.id.textViewNumber);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Contact currentContact = mContacts.get(position);
        viewHolder.textViewName.setText(currentContact.mName);
        viewHolder.textViewSurname.setText(currentContact.mSurname);
        viewHolder.textViewNumber.setText(currentContact.mNumber);
        return convertView;
    }

    public static class ViewHolder {
        public TextView textViewName;
        public TextView textViewSurname;
        public TextView textViewNumber;
    }

}
