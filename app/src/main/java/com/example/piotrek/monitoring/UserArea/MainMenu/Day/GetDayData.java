package com.example.piotrek.monitoring.UserArea.MainMenu.Day;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import com.example.piotrek.monitoring.Adapters.SectionsPageAdapter;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.data;
import static android.R.attr.password;

public class GetDayData extends AppCompatActivity {


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;

    private ViewPager mViewPager;
    private String currentDate, user_id, login, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_day_data);

        Intent intent = getIntent();
        currentDate = intent.getStringExtra("date");
        user_id = intent.getStringExtra("id");
        login = intent.getStringExtra("login");
        pass = intent.getStringExtra("pass");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setTitle("Informacja");

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout2);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.flContent2);
        mViewPager.setOffscreenPageLimit(6);

        setupViewPager(mViewPager);


        final NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_menu2);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.nav_home): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(0);
                        setTitle("Informacja");
                        break;
                    }
                    case (R.id.nav_basic): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(1);
                        setTitle("Podstawowe parametry");
                        break;
                    }
                    case (R.id.nav_day): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(2);
                        setTitle("Przebieg dnia");
                        break;
                    }
                    case (R.id.nav_back): {
                        finish();
                        break;
                    }
                    case (R.id.nav_notify): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(3);
                        setTitle("Przypomnienia");
                    }
                }
                return true;
            }
        });


    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeScreen(), "Informacja");
        adapter.addFragment(new BasicParameters(), "Podstawowe parametry");
        adapter.addFragment(new Day(), "Przebieg dnia");
        adapter.addFragment(new AddNotification(), "Przypomnienia");
        adapter.addFragment(new ParametersHelp(), "Pomoc");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 4) {
            setTitle("Podstawowe parametry");
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 1 || mViewPager.getCurrentItem() == 2 || mViewPager.getCurrentItem() == 3) {
            setTitle("Informacja");
            mViewPager.setCurrentItem(0);
        } else {
            finish();
        }
    }

    public ArrayList<String> getData() {
        ArrayList<String> data = new ArrayList<>();
        data.add(user_id);
        data.add(currentDate);
        return data;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
                Log.d("DUPA", "DUPA");
            }
        }
    }

}
