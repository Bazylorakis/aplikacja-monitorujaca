package com.example.piotrek.monitoring.UserArea.MainMenu.Day;


import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.piotrek.monitoring.R;

import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import java.text.SimpleDateFormat;
import java.util.Calendar;


import static android.R.attr.data;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import static java.lang.Integer.parseInt;

/**
 * Created by Piotrek on 06.01.2018.
 */

public class BasicParameters extends android.support.v4.app.Fragment {

    FloatingActionButton fab;
    EditText etTetno, etTemp, etOddech, etCisnienie;
    Button btnCommit;

    GetDayData getDayData;


    private int pulse = 0, breath = 0, skurcz = 0, rozkurcz = 0;
    double temp = 0.0;
    String sr = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.basic_parameters_fragment, container, false);


        getDayData = (GetDayData) getActivity();

        final String user_id = getDayData.getData().get(0);

        btnCommit = view.findViewById(R.id.addDayDatabtn);

        fab = view.findViewById(R.id.fabHelp);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager viewPager = getActivity().findViewById(R.id.flContent2);
                viewPager.setCurrentItem(4);
                getActivity().setTitle("Pomoc");
            }
        });

        etTetno = view.findViewById(R.id.etTetno);
        etTemp = view.findViewById(R.id.etTemperatura);
        etOddech = view.findViewById(R.id.etOddech);
        etCisnienie = view.findViewById(R.id.etCisnienie);

        btnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etTetno.getText().toString().matches(""))
                    pulse = parseInt(etTetno.getText().toString());
                if (!etTemp.getText().toString().matches("")) {
                    temp = Double.parseDouble(etTemp.getText().toString());
                }
                if (!etOddech.getText().toString().matches("")) {
                    breath = Integer.parseInt(etOddech.getText().toString());
                }
                if (!etCisnienie.getText().toString().matches("") && etCisnienie.getText().toString().contains("/")) {
                    sr = etCisnienie.getText().toString();
                    String[] parts = sr.split("/");
                    skurcz = Integer.parseInt(parts[0]);
                    rozkurcz = Integer.parseInt(parts[1]);
                }
                if (pulse == 0 || temp == 0.0 || breath == 0 || (skurcz == 0 || rozkurcz == 0)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Wypełnij wszystkie pola aby dane były dokładne!");
                    builder.create();
                    builder.show();

                } else {
                    GetDayData getDayData = (GetDayData) getActivity();
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                    df.setTimeZone(TimeZone.getTimeZone("GMT+01:00"));
                    String pure_data = getDayData.getData().get(1);
                    String data = df.format(c.getTime());
                    Log.d("CurrentTIme: ", data);
                    Intent intent = new Intent(getContext(), WaitInBackground.class);
                    intent.putExtra("pure_data", pure_data);
                    intent.putExtra("data", data);
                    if (pulse != 0)
                        intent.putExtra("pulse", pulse);
                    if (temp != 0)
                        intent.putExtra("temp", temp);
                    if (breath != 0)
                        intent.putExtra("breath", breath);
                    if (skurcz != 0 && rozkurcz != 0) {
                        intent.putExtra("skurcz", skurcz);
                        intent.putExtra("rozkurcz", rozkurcz);
                    }
                    intent.putExtra("user_id", user_id);
                    boolean sendDayData = true;
                    intent.putExtra("sendDayData", sendDayData);
                    etTetno.setText("");
                    etCisnienie.setText("");
                    etOddech.setText("");
                    etTemp.setText("");
                    pulse = 0;
                    temp = 0.0;
                    breath = 0;
                    skurcz = 0;
                    rozkurcz = 0;
                    startActivityForResult(intent, 1);
                }
            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Dodano pomyślnie!");
                builder.create();
                builder.show();
            } else if (resultCode == RESULT_CANCELED) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setMessage("Wystąpił błąd!");
                builder.create().show();
            }
        }
    }

 /*   @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }*/
}
