package com.example.piotrek.monitoring.UserArea.MainMenu.Day;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.example.piotrek.monitoring.R;

/**
 * Created by Piotrek on 07.01.2018.
 */

public class ParametersHelp extends android.support.v4.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.basic_parameters_help_fragment, container, false);


        return view;
    }


}
