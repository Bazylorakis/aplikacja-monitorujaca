package com.example.piotrek.monitoring.Data.Contact_data;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 11.01.2018.
 */

public class BasicReading implements Comparable<BasicReading>{
    public String mAcutalDate;
    public Date date;

    public BasicReading() {
    }

    public BasicReading(String dateOfReading) throws ParseException {
        this.mAcutalDate = dateOfReading; /*dateOfReading.substring(0, dateOfReading.indexOf(" ")) + " " + dateOfReading.substring(11);*/
        DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = sourceFormat.parse(this.mAcutalDate);
        this.date = date;
        Log.d("GETDATE", this.date.toString());

    }

    public BasicReading newReading(String dateOfReading) throws ParseException {
        BasicReading reading = new BasicReading(dateOfReading);
        return reading;
    }

    public ArrayList<BasicReading> getReadings() {
        ArrayList<BasicReading> readings = new ArrayList<>();
        return readings;
    }

    public Date getDate(){
        return date;
    }

    @Override
    public int compareTo(BasicReading o){
        return o.getDate().compareTo(getDate());
    }

}
