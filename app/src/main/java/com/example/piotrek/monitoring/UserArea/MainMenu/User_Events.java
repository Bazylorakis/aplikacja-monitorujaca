package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.piotrek.monitoring.BackgroundTasks.GetBasicReadingsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.GetEventsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveEventAsync;
import com.example.piotrek.monitoring.Data.Contact_data.Event;
import com.example.piotrek.monitoring.Data.Contact_data.User_events;
import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

import static android.app.Activity.RESULT_OK;
import static com.example.piotrek.monitoring.UserArea.MainMenu.User_Tab2.getBasicReadingsAsync;

/**
 * Created by Piotrek on 10.01.2018.
 */

public class User_Events extends android.support.v4.app.Fragment implements AsyncResponse {

    ListView listView;
    ArrayList<Event> events;
    User_events user_events;
    String user_id;
    Event event;
    FloatingActionButton fabRefresh;
    String email;

    private static final String TAG = "UserTabEvents";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_events_fragment, container, false);

        fabRefresh = view.findViewById(R.id.fabRefreshEvents);
        final GetEventsAsync getEventsAsync = new GetEventsAsync(getContext(), "getEvents");

        UserAreaActivity activity = (UserAreaActivity) getActivity();

        user_id = activity.getData().get(0);
        email = activity.getData().get(8);

        getEventsAsync.delegate = User_Events.this;
        try {
            getEventsAsync.execute(user_id);
        } catch (Exception e) {
            Log.d("Exception: ", e.toString());
        }

        listView = view.findViewById(R.id.lvEvents);
        event = new Event();
        events = event.getEvents();
        user_events = new User_events(getContext(), events, user_id);
        listView.setAdapter(user_events);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int pos = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Czy na pewno chcesz usunąć wpis?")
                        .setCancelable(false)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                RemoveEventAsync removeEventAsync = new RemoveEventAsync(getContext(), "removeEvent");
                                Event currentEvent = events.get(pos);
                                String date = currentEvent.mEventDate;
                                removeEventAsync.delegate = User_Events.this;
                                removeEventAsync.execute(user_id, date);
                                events.remove(pos);
                                listView.setAdapter(user_events);
                                user_events.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Event currentEvent = events.get(i);
                String title = currentEvent.mEventName;
                String fullDesc = currentEvent.mEventDesc;
                String date = currentEvent.mEventDate;
                Intent intent = new Intent(getContext(), EventFullInfo.class);
                intent.putExtra("title", title);
                intent.putExtra("full", fullDesc);
                intent.putExtra("date", date);
                intent.putExtra("contact", email);
                startActivityForResult(intent, 2);
            }
        });

        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                events.clear();
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                intent.putExtra("user_id", user_id);
                boolean requestRefreshEvents = true;
                intent.putExtra("requestRefreshEvents", requestRefreshEvents);
                startActivityForResult(intent, 1);
            }
        });

        return view;
    }

    @Override
    public void processStart(String output) {

    }

    @Override
    public void processFinish(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getEvents");
            if (response) {
                JSONArray arr = jsonObject.getJSONArray("events");
                for (int i = 0; i < arr.length(); i++) {
                    Log.d("W forze: ", arr.getJSONObject(i).getString("name"));
                    Log.d("W forze: ", arr.getJSONObject(i).getString("desc"));
                    Log.d("W forze: ", arr.getJSONObject(i).getString("date"));
                    String name = arr.getJSONObject(i).getString("name");
                    String desc = arr.getJSONObject(i).getString("desc");
                    String date = arr.getJSONObject(i).getString("date");
                    events.add(event.newEvent(name, desc, date));
                }
                Collections.sort(events);
                User_events user_events = new User_events(getContext(), events, user_id);
                listView.setAdapter(user_events);
                user_events.notifyDataSetChanged();
                listView.invalidateViews();
            } else {
                Toast.makeText(getContext(), "Brak wydarzeń.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("removedEvent");
            if (response) {
                Toast.makeText(getContext(), "Usunięto pomyślnie.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

/*    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(data.getStringExtra("output"));
                    JSONArray arr = jsonObject.getJSONArray("events");
                    for (int i = 0; i < arr.length(); i++) {
                        Log.d("W forze: ", arr.getJSONObject(i).getString("name"));
                        Log.d("W forze: ", arr.getJSONObject(i).getString("desc"));
                        Log.d("W forze: ", arr.getJSONObject(i).getString("date"));
                        String name = arr.getJSONObject(i).getString("name");
                        String desc = arr.getJSONObject(i).getString("desc");
                        String date = arr.getJSONObject(i).getString("date");
                        events.add(event.newEvent(name, desc, date));
                    }
                    Collections.sort(events);
                    User_events user_events = new User_events(getContext(), events, user_id);
                    listView.setAdapter(user_events);
                    user_events.notifyDataSetChanged();
                    listView.invalidateViews();
                    Toast.makeText(getContext(), "Odświeżono!", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {

            }
        }
    }
}
