package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;


import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.piotrek.monitoring.Adapters.NotificationDbAdapter;
import com.example.piotrek.monitoring.Adapters.SectionsPageAdapter;
import com.example.piotrek.monitoring.Data.Contact_data.UserData.UserProfile;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import static android.R.id.message;
import static com.example.piotrek.monitoring.R.id.etLogin;
import static com.example.piotrek.monitoring.R.id.etPassword;


public class UserAreaActivity extends AppCompatActivity {

    private static final String TAG = "UserActivity";

    private ViewPager mViewPager;


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;


    private String user_id;
    private String user_login;
    private String user_pass;
    private String user_email;
    private String user_name;
    private String user_surname;
    private String user_pesel;
    private String user_age;
    private String user_contact_email;
    private int user_first_login;

    UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_area);
        setTitle("Dodaj wyniki");

        Intent intent = getIntent();

        user_login = intent.getStringExtra("login");
        user_id = intent.getStringExtra("user_id");
        user_email = intent.getStringExtra("email");
        user_contact_email = intent.getStringExtra("contact");
        user_name = intent.getStringExtra("name");
        user_pass = intent.getStringExtra("password");
        user_surname = intent.getStringExtra("surname");
        user_pesel = intent.getStringExtra("pesel");
        user_age = intent.getStringExtra("age");
        user_first_login = intent.getIntExtra("first_login", 5);


        userProfile = new UserProfile(user_id, user_login, user_email, user_name, user_surname, user_pesel, user_age, user_first_login, user_contact_email);

        if (userProfile.mFirstTime == 1) {
            NotificationDbAdapter adapter = new NotificationDbAdapter(this);
            adapter.upgrade();
            Intent intent1 = new Intent(getApplicationContext(), FirstLoginInfo.class);
            intent1.putExtra("email", user_email);
            intent1.putExtra("id", user_id);
            startActivityForResult(intent1, 1);
        }


        toolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout1);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.flContent);
        mViewPager.setOffscreenPageLimit(7);
        setupViewPager(mViewPager);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


        final NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_menu);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.nav_data): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_data));
                        mViewPager.setCurrentItem(0);
                        setTitle("Dodaj wyniki");
                        break;
                    }
                    case (R.id.nav_stats): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_stats));
                        mViewPager.setCurrentItem(1);
                        setTitle("Statystyki");
                        break;
                    }
                    case (R.id.nav_period): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_period));
                        mViewPager.setCurrentItem(2);
                        setTitle("Statystyki okresowe");
                        break;
                    }
                    case (R.id.nav_reminders): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(3);
                        setTitle("Przypomnienia");
                        break;
                    }
                    case (R.id.nav_events): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        mViewPager.setCurrentItem(4);
                        setTitle("Wydarzenia");
                        break;
                    }
                    case (R.id.nav_contact): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_stats));
                        mViewPager.setCurrentItem(5);
                        setTitle("Szybki kontakt");
                        break;
                    }
                    case (R.id.nav_steps): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_stats));
                        mViewPager.setCurrentItem(6);
                        setTitle("Krokomierz");
                        break;
                    }
                    case (R.id.nav_settings): {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Log.d("Item id: ", Integer.toString(R.id.nav_stats));
                        mViewPager.setCurrentItem(7);
                        setTitle("Ustawienia");
                        break;
                    }
                    case (R.id.nav_logout): {
                        finish();
                        break;
                    }
                }
                return true;
            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new User_Tab1(), "Dodaj wyniki");
        adapter.addFragment(new User_Tab2(), "Statystyki");
        adapter.addFragment(new StatsFromPeriod(), "Statystyki okresowe");
        adapter.addFragment(new User_Reminders(), "Przypomnienia");
        adapter.addFragment(new User_Events(), "Wydarzenia");
        adapter.addFragment(new User_Tab3(), "Szybki Kontakt");
        adapter.addFragment(new User_Tab4(), "Prześlij zdjęcie");
        adapter.addFragment(new User_Tab5(), "Ustawienia konta");
        viewPager.setAdapter(adapter);
    }

    public ArrayList<String> getData() {
        ArrayList<String> data = new ArrayList<>();
        data.add(user_id);
        data.add(user_login);
        data.add(user_age);
        data.add(user_name);
        data.add(user_surname);
        data.add(user_pesel);
        data.add(user_email);
        data.add(user_pass);
        data.add(user_contact_email);
        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Log.d("UPDATE", "OK");
                Toast.makeText(getApplicationContext(), "Uaktualniono pomyślnie", Toast.LENGTH_SHORT).show();
            } else if (resultCode == 2) {
                finish();
            }
        }
        if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
                Log.d("UPDATE", "OK");
                Toast.makeText(getApplicationContext(), "Uaktualniono pomyślnie", Toast.LENGTH_SHORT).show();
            } else if (resultCode == 2) {
                finish();
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() != 0) {
            setTitle("Dodaj wyniki");
            mViewPager.setCurrentItem(0);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Czy na pewno chcesz wyjść?")
                    .setCancelable(false)
                    .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            UserAreaActivity.this.finish();
                        }
                    })
                    .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public Activity getActivity(Fragment fragment) {
        if (fragment == null) {
            return null;
        }
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getActivity();
    }


}
