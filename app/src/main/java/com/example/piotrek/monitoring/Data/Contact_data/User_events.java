package com.example.piotrek.monitoring.Data.Contact_data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static android.graphics.Color.rgb;

/**
 * Created by Piotrek on 11.01.2018.
 */

public class User_events extends BaseAdapter {

    Context mContext;
    ArrayList<Event> mEvents;
    LayoutInflater layoutInflater;
    String mUser_id;

    public User_events(Context c, ArrayList<Event> events, String userID) {
        mUser_id = userID;
        mContext = c;
        mEvents = events;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mEvents.size();
    }

    public Object getItem(int position) {
        return mEvents.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.event_list_view, null);
            viewHolder = new ViewHolder();

            viewHolder.textViewTitle = convertView.findViewById(R.id.tvEventName);
            viewHolder.textViewDesc = convertView.findViewById(R.id.tvShortDesc);
            viewHolder.textViewDate = convertView.findViewById(R.id.tvEventDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(position==0){ // for two rows if(position==0 || position==1)
            convertView.setBackgroundColor(rgb(211, 255, 242));
        }
        Event currentEvent = mEvents.get(position);
        viewHolder.textViewTitle.setText(currentEvent.mEventName);
        viewHolder.textViewDesc.setText(currentEvent.mEventDesc);
        viewHolder.textViewDate.setText(currentEvent.mEventDate);
        return convertView;
    }

    public static class ViewHolder {
        public TextView textViewTitle;
        public TextView textViewDesc;
        public TextView textViewDate;
    }

}

