package com.example.piotrek.monitoring.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.piotrek.monitoring.Data.Contact_data.UserNotification;
import com.example.piotrek.monitoring.R;

import java.util.ArrayList;

/**
 * Created by Piotrek on 19.01.2018.
 */

public class NotificationsAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<NotificationTemplate> mNotifications;
    LayoutInflater layoutInflater;
    String mUser_id;

    public NotificationsAdapter(Context c, ArrayList<NotificationTemplate> usernotifications, String user_id) {
        mUser_id = user_id;
        mContext = c;
        mNotifications = usernotifications;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mNotifications.size();
    }

    public Object getItem(int position) {
        return mNotifications.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.notification_item_view, null);
            viewHolder = new ViewHolder();
            viewHolder.textViewTitle = convertView.findViewById(R.id.tvNotiTitle);
            viewHolder.textViewTime = convertView.findViewById(R.id.tvNotiTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        NotificationTemplate currentNotification = mNotifications.get(position);
        viewHolder.textViewTitle.setText(currentNotification.mTitle);
        viewHolder.textViewTime.setText(currentNotification.mTime_of_notification);
        return convertView;
    }

    public static class ViewHolder {
        public TextView textViewTitle;
        public TextView textViewTime;
    }

}
