package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.piotrek.monitoring.Interfaces.RefreshMyData;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import static android.R.attr.data;
import static android.app.Activity.RESULT_OK;
import static com.example.piotrek.monitoring.R.id.btnAccept;
import static com.example.piotrek.monitoring.R.id.etLogin;
import static com.example.piotrek.monitoring.R.id.etPassword;
import static com.example.piotrek.monitoring.R.id.start;
import static com.example.piotrek.monitoring.R.id.textView;
import static java.security.AccessController.getContext;

/**
 * Created by Piotrek on 27.10.2017.
 */

public class User_Tab5 extends Fragment {


    private TextView tvImie, tvWiek, tvPesel, tvEmail;
    private EditText etName, etSurname, etAge, etPesel, etEmail, etContact;
    Button btn;
    UserAreaActivity userAreaActivity;
    String user_id, user_pass, user_login;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_tab5, container, false);


        userAreaActivity = (UserAreaActivity) getActivity();

        user_id = userAreaActivity.getData().get(0);
        user_pass = userAreaActivity.getData().get(7);
        user_login = userAreaActivity.getData().get(1);


        btn = view.findViewById(R.id.btnAccept);

        tvImie = view.findViewById(R.id.tvImie);
        tvWiek = view.findViewById(R.id.tvWiek);
        tvPesel = view.findViewById(R.id.tvPesel);
        tvEmail = view.findViewById(R.id.tvEmail);

        etContact = view.findViewById(R.id.etContactEmailUpdate);
        etName = view.findViewById(R.id.etName);
        etSurname = view.findViewById(R.id.etSurname);
        etAge = view.findViewById(R.id.etAge);
        etPesel = view.findViewById(R.id.etPesel);
        etEmail = view.findViewById(R.id.etEmailUpdate);

        etName.setText(userAreaActivity.getData().get(3));
        etSurname.setText(userAreaActivity.getData().get(4));
        etAge.setText(userAreaActivity.getData().get(2));
        etPesel.setText(userAreaActivity.getData().get(5));
        etEmail.setText(userAreaActivity.getData().get(6));
        etContact.setText(userAreaActivity.getData().get(8));

        tvWiek.setText("Wiek :" + userAreaActivity.getData().get(2));
        tvImie.setText(userAreaActivity.getData().get(3) + " " + userAreaActivity.getData().get(4));
        tvPesel.setText("PESEL: " + userAreaActivity.getData().get(5));
        tvEmail.setText(userAreaActivity.getData().get(6));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("etName", etName.getText().toString());
                Intent intent = new Intent(getContext(), WaitInBackground.class);
                intent.putExtra("name", etName.getText().toString());
                intent.putExtra("surname", etSurname.getText().toString());
                intent.putExtra("age", etAge.getText().toString());
                intent.putExtra("pesel", etPesel.getText().toString());
                intent.putExtra("email", etEmail.getText().toString());
                intent.putExtra("user_id", user_id);
                intent.putExtra("contact", etContact.getText().toString());
                boolean requestUpdate = true;
                intent.putExtra("requestUpdate", requestUpdate);
                startActivityForResult(intent, 11);
            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 11) {
            if (resultCode == RESULT_OK) {

                Intent intent = new Intent(getContext(), UserAreaActivity.class);
                intent.putExtra("name", etName.getText().toString());
                intent.putExtra("surname", etSurname.getText().toString());
                intent.putExtra("age", etAge.getText().toString());
                intent.putExtra("pesel", etPesel.getText().toString());
                intent.putExtra("email", etEmail.getText().toString());
                intent.putExtra("user_id", user_id);
                intent.putExtra("contact", etContact.getText().toString());
                userAreaActivity.finish();
                startActivity(intent);
            } else if (resultCode == 1137) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setMessage("Wystąpił błąd!");
                builder.create();
            }
        }
    }

    public void run() {
        Intent intent = getActivity().getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();

        getActivity().overridePendingTransition(0, 0);
        startActivity(intent);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Zmieniono pomyślnie!").create();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}
