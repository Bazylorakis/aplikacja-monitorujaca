package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.piotrek.monitoring.Adapters.NotificationDbAdapter;
import com.example.piotrek.monitoring.Adapters.NotificationTemplate;
import com.example.piotrek.monitoring.Adapters.NotificationsAdapter;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveBasicReadingAsync;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReading;
import com.example.piotrek.monitoring.Data.Contact_data.BasicReadingAdapter;
import com.example.piotrek.monitoring.Data.Contact_data.UserNotification;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.Day.Receiver;
import com.example.piotrek.monitoring.UserInterface.WaitInBackground;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by Piotrek on 10.01.2018.
 */

public class User_Reminders extends android.support.v4.app.Fragment {

    ListView listView;
    ArrayList<NotificationTemplate> notifications;
    NotificationsAdapter notificationsAdapter;
    String user_id;
    NotificationTemplate notification;
    FloatingActionButton fabRefresh;

    private static final String TAG = "UserTabReminders";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_reminders_fragment, container, false);

        UserAreaActivity userAreaActivity = (UserAreaActivity) getActivity();
        user_id = userAreaActivity.getData().get(0);

        fabRefresh = view.findViewById(R.id.fabRefreshReminders);

        notifications = new ArrayList<>();

        listView = view.findViewById(R.id.lvReminders);
        notification = new NotificationTemplate();
        notifications = notification.getNotifications();



        final NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
        adapter.open();
        final Cursor dbCursor = adapter.getUserNotifications(user_id);
        if (dbCursor.moveToFirst()) {
            do {
                try {
                    NotificationTemplate template = adapter.getNotificationFromCursor(dbCursor);
                    notifications.add(new NotificationTemplate(template.mUserId, template.mTitle, template.mTime_of_notification, template.mRandomId));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (dbCursor.moveToNext());
            notificationsAdapter = new NotificationsAdapter(getContext(), notifications, user_id);
            listView.setAdapter(notificationsAdapter);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int pos = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Czy na pewno chcesz usunąć przypomnienie?")
                        .setCancelable(false)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                NotificationTemplate currentNotification = notifications.get(pos);
                                adapter.deleteNotification(user_id, currentNotification.mTitle);
                                notifications.remove(pos);
                                listView.setAdapter(notificationsAdapter);
                                notificationsAdapter.notifyDataSetChanged();
                                Intent intent = new Intent(getContext(), Receiver.class);
                                PendingIntent pendingIntent =
                                        PendingIntent
                                                .getBroadcast
                                                        (getContext(), currentNotification.mRandomId,
                                                                intent, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager alarmManager =
                                        (AlarmManager) getContext()
                                                .getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(pendingIntent);
                            }

                        })
                        .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int notifications_length = notifications.size();
                notifications.clear();
                final NotificationDbAdapter adapter = new NotificationDbAdapter(getContext());
                adapter.open();
                final Cursor dbCursor = adapter.getUserNotifications(user_id);
                if (dbCursor.moveToFirst()) {
                    do {
                        try {
                            NotificationTemplate template = adapter.getNotificationFromCursor(dbCursor);
                            notifications.add
                                    (new NotificationTemplate
                                            (template.mUserId, template.mTitle, template.mTime_of_notification, template.mRandomId));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } while (dbCursor.moveToNext());
                    notificationsAdapter = new NotificationsAdapter(getContext(), notifications, user_id);
                    final NotificationsAdapter notificationsAdapter = new NotificationsAdapter(getContext(), notifications, user_id);
                    listView.setAdapter(notificationsAdapter);
                    int notifications_length2 = notifications.size();
                    if(notifications_length == notifications_length2)
                        Toast.makeText(getContext(), "Brak zmian", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), "Zaktualizowano", Toast.LENGTH_SHORT).show();

                }}});
return view;

    }
}
