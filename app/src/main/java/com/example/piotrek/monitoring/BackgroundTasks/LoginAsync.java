package com.example.piotrek.monitoring.BackgroundTasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Piotrek on 07.01.2018.
 */

public class LoginAsync extends AsyncTask<String, Void, String> {

    public AsyncResponse delegate = null;

    Context ctx;
    String action;

    public LoginAsync(Context c, String type) {
        this.ctx = c;
        this.action = type;
    }

    protected void onPreExecute() {
        String start = "start";
        delegate.processStart(start);
    }

    protected String doInBackground(String... params) {
        if (action == "login") {
            try {
                URL url = new URL("https://monitoring-projekt.000webhostapp.com/login.php");
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("user", params[0]);
                postDataParams.put("pass", params[1]);
                Log.e("Params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader inp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = inp.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    inp.close();
                    return sb.toString();
                } else
                    return new String("False: " + responseCode);
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }
        return "Exit";
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("Result:", result);
        try {
            JSONObject jsonObject = new JSONObject(result);
            Boolean response = jsonObject.getBoolean("success_login");
            if (response) {
                delegate.processFinish(result);
            } else {
                delegate.processFinish(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }


}
