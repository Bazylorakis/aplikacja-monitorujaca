package com.example.piotrek.monitoring.UserArea.MainMenu.Day;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import org.w3c.dom.Text;

/**
 * Created by Piotrek on 10.01.2018.
 */

public class HomeScreen extends android.support.v4.app.Fragment {

    TextView tvMessage;

    GetDayData getDayData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_screen_fragment, container, false);

        getDayData = (GetDayData) getActivity();

        tvMessage = view.findViewById(R.id.tvSelectedDate);

        tvMessage.setText("Wypełniasz dane dla dnia: " + getDayData.getData().get(1));

        return view;
    }
}
