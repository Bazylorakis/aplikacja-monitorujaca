package com.example.piotrek.monitoring.UserArea.MainMenu.Statistics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Piotrek on 13.01.2018.
 */

public class BreathView extends android.support.v4.app.Fragment {


    ArrayList<Integer> breath;
    ArrayList<Double> time;

    GraphView graphView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_breath_view, container, false);


        graphView = view.findViewById(R.id.breathGraph);


        Intent intent = getActivity().getIntent();

        String array = intent.getStringExtra("array");


        breath = new ArrayList<>();
        time = new ArrayList<>();


        try {
            LineGraphSeries<DataPoint> series;
            JSONArray jsonArray = new JSONArray(array);
            //  Log.d("JSONARRAY",jsonArray.getJSONObject(0).getString("pulse"));
            String date = jsonArray.getJSONObject(0).getString("time_of_reading");
            Log.d("Data:", date);
            for (int i = 0; i < jsonArray.length(); i++) {
                Log.d("Json jestem", "Tak");
                if (!jsonArray.getJSONObject(i).getString("breath").matches(""))
                    breath.add(Integer.parseInt(jsonArray.getJSONObject(i).getString("breath")));
                if (!jsonArray.getJSONObject(i).getString("time_of_reading").matches("")){
                    String toReplace = jsonArray.getJSONObject(i).getString("time_of_reading");
                char[] replace = toReplace.toCharArray();
                replace[2] = '.';
                toReplace = String.valueOf(replace);
                time.add(Double.parseDouble(toReplace));
                Log.d("Json jestem", Double.toString(time.get(i)));
                }
            }


            int size = breath.size();
            DataPoint[] values = new DataPoint[size];
            for (int i = 0; i < size; i++) {
                Double xi = time.get(i);
                Integer yi = breath.get(i);
                DataPoint v = new DataPoint(xi, yi);
                values[i] = v;
            }
            series = new LineGraphSeries<DataPoint>(values);

            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMinY(5);
            graphView.getViewport().setMaxY(30);

            graphView.getViewport().setXAxisBoundsManual(true);
            graphView.getViewport().setMinX(0);
            graphView.getViewport().setMaxX(24);


            series.setDrawDataPoints(true);

            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getContext(), "Oddech.\nGodzina: " + dataPoint.getX() +"\nWartość: "+ dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });

            graphView.getGridLabelRenderer().setNumHorizontalLabels(24);

            graphView.addSeries(series);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
