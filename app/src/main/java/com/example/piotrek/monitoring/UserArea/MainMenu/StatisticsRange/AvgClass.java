package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import java.util.Date;

/**
 * Created by Piotrek on 23.01.2018.
 */

public class AvgClass {
    int avgPulse;
    int avgBreath;
    int avgSkurcz;
    int avgRozkurcz;
    Date date;
    double avgTemps;


    public AvgClass(Date d, int pulse, int breath, int skurcz, int rozkurcz, double temps){
        this.avgPulse = pulse;
        this.date = d;
        this.avgBreath = breath;
        this.avgSkurcz = skurcz;
        this.avgTemps = temps;
        this.avgRozkurcz = rozkurcz;
    }
}
