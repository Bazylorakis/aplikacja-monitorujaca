package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;


import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserArea.MainMenu.Day.GetDayData;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;


/**
 * Created by Piotrek on 27.10.2017.
 */

public class User_Tab1 extends android.support.v4.app.Fragment {

    private static final String TAG = "UserTab1Fragment";

    CalendarView calendar;
    Button btnYesterday, btnToday, btnTomorrow;
    UserAreaActivity userAreaActivity;

    String user_id, login, pass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_tab1, container, false);

        userAreaActivity = (UserAreaActivity) getActivity();

        user_id = userAreaActivity.getData().get(0);
        login = userAreaActivity.getData().get(1);
        pass = userAreaActivity.getData().get(7);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);


        calendar = view.findViewById(R.id.calendar);



        btnToday = view.findViewById(R.id.today);
        btnYesterday = view.findViewById(R.id.yesterday);
        btnTomorrow = view.findViewById(R.id.tomorrow);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                int d = dayOfMonth;
                int m = month;
                int y = year;
                String curDate;
                if (d >= 10 && m + 1 >= 10) {
                    curDate = String.valueOf(d) + "-" + String.valueOf(m + 1) + "-" + String.valueOf(y);
                } else if (d >= 10 && m + 1 < 10) {
                    curDate = String.valueOf(d) + "-" + "0" + String.valueOf(m + 1) + "-" + String.valueOf(y);
                } else if(d < 10 && m+1 >= 10){
                    curDate = "0" + String.valueOf(d) + "-" + String.valueOf(m+1) + "-" + String.valueOf(y);
                }
                else {
                    curDate = "0" + String.valueOf(d) + "-" + "0" + String.valueOf(m + 1) + "-" + String.valueOf(y);
                }
                Intent intent = new Intent(getContext(), GetDayData.class);
                intent.putExtra("date", curDate);
                intent.putExtra("id", user_id);
                startActivityForResult(intent, 9);
            }
        });


        btnToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String data = df.format(c.getTime());
                Intent intent = new Intent(getContext(), GetDayData.class);
                intent.putExtra("date", data);
                intent.putExtra("id", user_id);
                startActivityForResult(intent, 9);
            }
        });

        btnTomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, 1);
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String data = df.format(c.getTime());
                Log.d("Klanedarz", data);
                Intent intent = new Intent(getContext(), GetDayData.class);
                intent.putExtra("date", data);
                intent.putExtra("id", user_id);
                startActivityForResult(intent, 9);
            }
        });

        btnYesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, -1);
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String data = df.format(c.getTime());
                Log.d("Klanedarz", data);
                Intent intent = new Intent(getContext(), GetDayData.class);
                intent.putExtra("date", data);
                intent.putExtra("id", user_id);
                startActivityForResult(intent, 9);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public ArrayList<String> getData() {
        ArrayList<String> data = new ArrayList<>();
        data.add(user_id);
        return data;
    }

}


