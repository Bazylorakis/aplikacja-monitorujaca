package com.example.piotrek.monitoring.UserArea.MainMenu.StatisticsRange;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.piotrek.monitoring.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Piotrek on 22.01.2018.
 */

public class PulseViewLong extends android.support.v4.app.Fragment {


    ArrayList<Integer> pulse;
    ArrayList<Date> time;

    GraphView graphView;
    DateFormat sdf;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_breath_view_long, container, false);


        StatisticsRangeMenu statisticsRangeMenu = (StatisticsRangeMenu) getActivity();



        graphView = view.findViewById(R.id.breathGraph);




        pulse = new ArrayList<>();
        time = new ArrayList<>();

        for(int i = 0; i<statisticsRangeMenu.averages.size(); i++){
            pulse.add(statisticsRangeMenu.averages.get(i).avgPulse);
            time.add(statisticsRangeMenu.averages.get(i).date);
        }


            LineGraphSeries<DataPoint> series;

            int size = pulse.size();
            DataPoint[] values = new DataPoint[size];
            for (int i = 0; i < size; i++) {
                Date xi = time.get(i);
                Integer yi = pulse.get(i);
                DataPoint v = new DataPoint(xi, yi);
                values[i] = v;
            }
            series = new LineGraphSeries<DataPoint>(values);

        graphView.addSeries(series);

            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMinY(0);
            graphView.getViewport().setMaxY(240);


      sdf = new SimpleDateFormat("dd-MM-yyyy");



    graphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getContext(), sdf));
    graphView.getGridLabelRenderer().setNumHorizontalLabels(time.size());

        graphView.getViewport().setMinX(series.getLowestValueX());
        graphView.getViewport().setMaxX(series.getHighestValueX());
        graphView.getViewport().setXAxisBoundsManual(true);

        graphView.getGridLabelRenderer().setHumanRounding(false);


            series.setDrawDataPoints(true);

            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getContext(), "Średnie tętno.\nData: " + sdf.format(dataPoint.getX())+"\nWartość: "+ dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });



        return view;

        }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
