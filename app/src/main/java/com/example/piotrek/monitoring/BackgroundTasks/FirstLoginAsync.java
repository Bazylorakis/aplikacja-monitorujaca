package com.example.piotrek.monitoring.BackgroundTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.piotrek.monitoring.Interfaces.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import static android.R.attr.action;

/**
 * Created by Piotrek on 07.01.2018.
 */

public class FirstLoginAsync extends AsyncTask<String, Void, String> {

    public AsyncResponse delegate = null;


    Context ctx;
    String action;

    public FirstLoginAsync(Context c, String type) {
        this.ctx = c;
        this.action = type;
    }

    protected void onPreExecute() {
        String start = "start";
        delegate.processStart(start);
    }

    protected String doInBackground(String... params) {
        if (action == "firstLogin") {
            try {
                URL url = new URL("https://monitoring-projekt.000webhostapp.com/firstLoginUpdate.php");
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("user_id", params[0]);
                postDataParams.put("name", params[1]);
                postDataParams.put("surname", params[2]);
                postDataParams.put("email", params[3]);
                postDataParams.put("age", params[4]);
                postDataParams.put("pesel", params[5]);
                postDataParams.put("first_login", params[6]);
                postDataParams.put("contact", params[7]);
                Log.e("Params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader inp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = inp.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    inp.close();
                    return sb.toString();
                } else
                    return new String("False: " + responseCode);
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }
        return "Exit";
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            Boolean response = jsonObject.getBoolean("updated");
            if (response) {
                delegate.processFinish(result);
            } else {
                delegate.processFinish(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }

}
