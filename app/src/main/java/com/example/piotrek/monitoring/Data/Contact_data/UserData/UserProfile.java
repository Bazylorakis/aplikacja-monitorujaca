package com.example.piotrek.monitoring.Data.Contact_data.UserData;

import com.example.piotrek.monitoring.UserArea.MainMenu.UserAreaActivity;

import static android.R.attr.name;

/**
 * Created by Piotrek on 07.01.2018.
 */

public class UserProfile {
    public String mID;
    public String mEmail;
    public String mLogin;
    public String mName;
    public String mSurname;
    public String mPesel;
    public String mAge;
    public int mFirstTime;
    public String mMedic;

    public UserProfile() {
    }

    public UserProfile(String id, String login, String email, String name, String surname, String pesel, String age, int firstTime, String medic) {
        this.mID = id;
        this.mLogin = login;
        this.mEmail = email;
        this.mName = name;
        this.mSurname = surname;
        this.mPesel = pesel;
        this.mAge = age;
        this.mFirstTime = firstTime;
        this.mMedic = medic;
    }

    public String items() {
        return this.mID + this.mEmail + this.mLogin + this.mName + this.mSurname + this.mPesel + this.mAge + this.mFirstTime;
    }


}
