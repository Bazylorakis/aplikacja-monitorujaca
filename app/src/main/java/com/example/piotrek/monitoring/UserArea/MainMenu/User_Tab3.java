package com.example.piotrek.monitoring.UserArea.MainMenu;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import android.widget.ListView;
import android.widget.Toast;


import com.example.piotrek.monitoring.BackgroundTasks.GetContactsAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveContactAsync;
import com.example.piotrek.monitoring.BackgroundTasks.RemoveEventAsync;
import com.example.piotrek.monitoring.Data.Contact_data.Contact;
import com.example.piotrek.monitoring.Data.Contact_data.Event;
import com.example.piotrek.monitoring.Data.Contact_data.User_contacts;

import com.example.piotrek.monitoring.Interfaces.AsyncResponse;
import com.example.piotrek.monitoring.MainApp.Tab2Fragment;
import com.example.piotrek.monitoring.R;
import com.example.piotrek.monitoring.UserInterface.AddNumberPop;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.R.attr.data;
import static android.app.Activity.RESULT_OK;


/**
 * Created by Piotrek on 27.10.2017.
 */

public class User_Tab3 extends Fragment implements AsyncResponse {

    private static final String TAG = "UserTab3Fragment";

    ListView listView;
    ArrayList<Contact> contacts;
    FloatingActionButton fab;
    User_contacts userContacts;
    String user_id;
    Contact contact;

    GetContactsAsync getContactsAsync = new GetContactsAsync(getContext(), "getContacts");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_tab3, container, false);

        UserAreaActivity activity = (UserAreaActivity) getActivity();

        user_id = activity.getData().get(0);


        fab = view.findViewById(R.id.fabAdd);

        getContactsAsync.delegate = User_Tab3.this;
        try {
            getContactsAsync.execute(user_id);
        } catch (Exception e) {
            Log.d("Exception: ", e.toString());
        }

        listView = view.findViewById(R.id.lvItems);
        contact = new Contact();
        contacts = contact.getContacts();
        userContacts = new User_contacts(getContext(), contacts, user_id);
        listView.setAdapter(userContacts);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contact currentContact = contacts.get(i);
                String numer = currentContact.mNumber.toString();
                Log.d("Call:", numer);
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + numer));
                startActivity(callIntent);
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int pos = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Czy na pewno chcesz usunąć wpis?")
                        .setCancelable(false)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                RemoveContactAsync removeContact = new RemoveContactAsync(getContext(), "removeContact");
                                Contact currentContact = contacts.get(pos);
                                String numer = currentContact.mNumber.toString();
                                removeContact.delegate = User_Tab3.this;
                                removeContact.execute(user_id, numer);
                                contacts.remove(pos);
                                listView.setAdapter(userContacts);
                                userContacts.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddNumberPop.class);
                intent.putExtra("user_id", user_id);
                startActivityForResult(intent, 10);
            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String surname = data.getStringExtra("surname");
                String number = data.getStringExtra("number");
                contacts.add(contact.newContact(name, surname, number));
                User_contacts user_contacts = new User_contacts(getContext(), contacts, user_id);
                listView.setAdapter(user_contacts);
                user_contacts.notifyDataSetChanged();
                Toast.makeText(getContext(), "Dodano pomyślnie", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void processFinish(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("success_getContacts");
            if (response) {
                JSONArray arr = jsonObject.getJSONArray("contacts");
                for (int i = 0; i < arr.length(); i++) {
                    String name = arr.getJSONObject(i).getString("name");
                    String surname = arr.getJSONObject(i).getString("surname");
                    String number = arr.getJSONObject(i).getString("number");
                    contacts.add(contact.newContact(name, surname, number));
                }
                User_contacts user_contacts = new User_contacts(getContext(), contacts, user_id);
                listView.setAdapter(user_contacts);
                user_contacts.notifyDataSetChanged();
            } else {
                Toast.makeText(getContext(), "Możesz uzupełnić kontakty w zakładce - Szybki kontakt", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(output);
            Boolean response = jsonObject.getBoolean("removed");
            if (response) {
                Toast.makeText(getContext(), "Usunięto pomyślnie.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processStart(String input) {
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
