package com.example.piotrek.monitoring.BackgroundTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.piotrek.monitoring.Interfaces.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import static android.R.attr.data;
import static android.R.attr.id;

/**
 * Created by Piotrek on 10.01.2018.
 */

public class BasicParamsUploadAsync extends AsyncTask<String, Void, String> {

    public AsyncResponse delegate = null;

    Context ctx;
    String action;

    public BasicParamsUploadAsync(Context c, String type) {

        this.ctx = c;
        this.action = type;
    }

    protected void onPreExecute() {
        String start = "start";
        delegate.processStart(start);
    }

    @Override
    protected String doInBackground(String... params) {
        if (action == "sendDayData") {
            try {

                String id = params[0];
                String data = params[1];
                String pulse = params[2];
                String breath = params[3];
                String skurcz = params[4];
                String rozkurcz = params[5];
                String temp = params[6];
                String pure_data = params[7];
                Log.d("Pure:", pure_data);

                URL url = new URL("https://monitoring-projekt.000webhostapp.com/sendDayData.php");

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("user_id", id);
                postDataParams.put("date", data);
                postDataParams.put("pulse", pulse);
                postDataParams.put("breath", breath);
                postDataParams.put("skurcz", skurcz);
                postDataParams.put("rozkurcz", rozkurcz);
                postDataParams.put("temp", temp);
                postDataParams.put("pure_data", pure_data);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();
                Log.e("Params", postDataParams.toString());

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader inp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = inp.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    inp.close();
                    return sb.toString();
                } else
                    return new String("False: " + responseCode);
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("Result data: ", result);
        delegate.processFinish(result);
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }
}
