package com.example.piotrek.monitoring.Interfaces;

import android.app.Activity;

/**
 * Created by Piotrek on 07.01.2018.
 */

public interface RefreshMyData {
    void refreshActivity(Activity app);
}
