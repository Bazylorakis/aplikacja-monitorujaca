package com.example.piotrek.monitoring.Data.Contact_data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piotrek.monitoring.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static android.graphics.Color.rgb;

/**
 * Created by Piotrek on 11.01.2018.
 */

public class BasicReadingAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<BasicReading> mReadings;
    LayoutInflater layoutInflater;
    String mUser_id;

    public BasicReadingAdapter(Context c, ArrayList<BasicReading> readings, String user_id) {
        mUser_id = user_id;
        mContext = c;
        mReadings = readings;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mReadings.size();
    }

    public Object getItem(int position) {
        return mReadings.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.readings_list_view, null);
            viewHolder = new ViewHolder();
            viewHolder.textViewDate = convertView.findViewById(R.id.tvDateInfo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        BasicReading currentReading = mReadings.get(position);
        viewHolder.textViewDate.setText(currentReading.mAcutalDate /*currentReading.mAcutalDate.substring(0, currentReading.mAcutalDate.indexOf(" "))*/);
        if(position==0){ // for two rows if(position==0 || position==1)
            convertView.setBackgroundColor(rgb(211, 255, 242));
        }
        return convertView;
    }

    public static class ViewHolder {
        public TextView textViewDate;
    }

}
